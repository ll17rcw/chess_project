# Generated by Django 3.2.8 on 2022-04-13 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chess_site', '0011_game_time_started'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='time_black',
            field=models.IntegerField(default=-1),
        ),
        migrations.AddField(
            model_name='game',
            name='time_white',
            field=models.IntegerField(default=-1),
        ),
    ]
