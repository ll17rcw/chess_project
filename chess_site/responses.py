from distutils.log import error
from enum import Enum, auto
from multiprocessing.sharedctypes import Value
from rest_framework.response import Response
from numpy import kaiser



success_responses = {
    "USER_CREATED" : {
        "details" : "User has been created.",
        "status" : 201
    },
    "LOGIN_SUCCESSFUL" : {
        "details" : "Login was successful.",
        "status" : 200
    },
    "GAME_CREATED" : {
        "details" : "Game was created successfully.",
        "status" : 201
    },
    "MOVE_MADE" : {
        "details" : "A move was made.",
        "status" : 200
    },
    "ALL_GAMES_RESPONSE" : {
        "details" : "List of all games that match query.",
        "status" : 200
    },
    "GAME_STATE_RESPONSE" : {
        "details" : "The state / details of a game",
        "status" : 200
    },
    "CLOCK_STATE_RESPONSE" : {
        "details" : "The state / details of a games clocks",
        "status" : 200
    },
    "GAME_CLOCK_STATE_RESPONSE" : {
        "details" : "The state / details of a game and its clocks.",
        "status" : 200
    },
    "VALID_FEN" : {
        "details" : "The FEN provided is valid.",
        "status" : 200
    },
    "VALID_MOVE" : {
        "details" : "Move provided is valid.",
        "status" : 200
    },
    "GAME_OVER" : {
        "details" : "The game is over.",
        "status" : 200
    }

}

error_responses = {
    "USER_EXISTS" : {
        "details" : "A user with them details already exists.",
        "status" : 400
    },
    "GAME_ALREADY_EXISTS" : {
        "details" : "That user already has an existing active game.",
        "status" : 400
    },
    "NOT_ENOUGH_USER_INFO": {
        "details" : "Not enough user information has been provided.",
        "status" : 400
    },
    "INVALID_USER_INFO" : {
        "details" : "The user information provided is invalid.",
        "status" : 400
    },
    "NOT_ENOUGH_GAME_INFO" : {
        "details" : "Not enough game information has been provided.",
        "status" : 400  
    },
    "INVALID_GAME_INFO" : {
        "details" : "The game information provided is invalid.",
        "status" : 400
    },
    "LOGIN_FAILED" : {
        "details" : "Login attempt failed.",
        "status" : 400
    },
    "GAME_NOT_FOUND" : {
        "details" : "No game has been found.",
        "status" : 404
    },
    "INVALID_FEN" : {
        "details" : "That FEN is invalid.",
        "status" : 400
    },
    "INVALID_MOVE" : {
        "details" : "Move provided is not valid.",
        "status" : 400
    }
    
}



class NameEnum(Enum):
    def _generate_next_value_(name, start, count, last_values):
        return str(name)

class ResponseEnum(NameEnum):
    pass

class SuccessResponse(ResponseEnum):
    USER_CREATED = auto()
    LOGIN_SUCCESSFUL = auto()
    GAME_CREATED = auto()
    MOVE_MADE = auto()
    ALL_GAMES_RESPONSE = auto()
    GAME_STATE_RESPONSE = auto()
    CLOCK_STATE_RESPONSE = auto()
    VALID_FEN = auto()
    VALID_MOVE = auto()
    GAME_OVER = auto()

class ErrorResponse(ResponseEnum):
    USER_EXISTS = auto()
    GAME_ALREADY_EXISTS = auto()
    NOT_ENOUGH_USER_INFO = auto()
    INVALID_USER_INFO = auto()
    NOT_ENOUGH_GAME_INFO = auto()
    INVALID_GAME_INFO = auto()
    LOGIN_FAILED = auto()
    GAME_NOT_FOUND = auto()
    INVALID_FEN = auto()
    INVALID_MOVE = auto()


    

class CustomResponse(Response):
    """
    Handles the generation and lookup of the appropriate Response from the 
    responses json files.
    """
    def __init__(self, response_key : ResponseEnum, details : str = None, data : dict=None):
        """
        Args:
            response_key (ResponseEnum): The key to search for the error code with.
            details (str): Optional details for overriding default details for specific cases.

        Raises:
            ValueError
        """
        if not isinstance(response_key, ResponseEnum):
            raise ValueError("Response key should be a ResponseEnum type.")
        if isinstance(response_key, ErrorResponse):
            responses = error_responses
        elif isinstance(response_key, SuccessResponse):
            responses = success_responses
        else:
            raise ValueError("Response key not found in responses dictionaries. Have you added it?")
        
        key = str(response_key.value)
        id = None
        if not (key in responses.keys()):
            raise ValueError("Key not found.")
        self.enum_value = response_key
        self.id = key
        self.details = responses[key]["details"] if details is None else details # This is so details can be overriden, if necessary.
        status = responses[key]["status"]

        super().__init__(data={"response_id" : self.id, 
                               "details" : self.details,
                               "response_body" : data}, status=status)
        