$('#newgame-form').on('submit', function(e){
    console.log("made it here");
    e.preventDefault();
    
    $.ajax({
          type : "POST", 
          url: "{% url 'newgame' %}",
          data: {
          first_name : $('#first_name').val(),
          last_name : $('#last_name').val(),
          csrfmiddlewaretoken: '{{ csrf_token }}',
          dataType: "json",
  
          },
          
          success: function(data){
            $('#output').html(data.msg) /* response message */
          },
  
          failure: function() {
              
          }
  
  
      });
  
  
          });