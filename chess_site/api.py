from django.db import IntegrityError
from django.http import HttpResponseServerError
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from chess_site.chessgamesmanager import ChessGamesManager
from chess_site.models import Game, GameStatus, TimeControl
from chess_site.responses import CustomResponse, ErrorResponse, SuccessResponse


from chess_site.views import get_active_game_of_user, get_active_game_of_user_with_colour
from chess_site.chessclock import ChessClock
from .serializers import ChessGameSerializer
from chess_site.chessvalidator import ChessValidator, InvalidBoardError, InvalidMoveError


from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from chess_site.serializers import UserSerializer, ChessGameSerializer
from rest_framework.views import APIView

import time



########## User ##########
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


########### Games & Clocks ###########

class GamesApiView(APIView):
    def get_games(self, awaiting_player, time_, increment):
        
        if awaiting_player == "white":
            games = Game.objects.filter(player_w=None)

        elif awaiting_player == "black":
            games = Game.objects.filter(player_b=None)
        else:
            games = Game.objects.all()
        if time_ and increment:
            game_time = TimeControl.objects.get(time_, increment)
            games = games.filter(game_time=game_time)
        return games
        
        
    def get(self, request):
        time_received = time.time_ns() // 1_000_000
        
        # return_data = {}
        awaiting_player = request.query_params.get("awaiting_player")
        time_ = request.query_params.get("time")
        increment = request.query_params.get("increment")
        time_with = request.query_params.get("time_with") 
        time_with = time_with if time_with else False
        games = self.get_games(awaiting_player, time_, increment)
        games_serialized = ChessGameSerializer(games, many=True, time_with=time_with)
        print("games on server\n\n\n{}\n\n\n".format(games_serialized.data))

        # for index, game in enumerate(games):
        #     game_serialized = ChessGameSerializer(game)
        #     return_data[index] = {}(ga
        #     return_data[index]['game'] = game_serialized.data
        #     if with_clock:
        #         white_clock, black_clock, time_now = ChessClock.calculate_clocks(game, time_received=time_received)
        #         return_data[index]['clock'] = {
        #             'white_clock' : white_clock,
        #             'black_clock' : black_clock,
        #             'time_calculated_with' : time_now
        #         }
        return CustomResponse(SuccessResponse.ALL_GAMES_RESPONSE, data=games_serialized.data)


class GameApiView(APIView):
    def get(self, request, id=None, format=None):
        """
        Returns details about a game for a logged in user. 

        Args:
            request : request data
            format (_type_, optional): _description_. Defaults to None.

        Returns:
            _type_: _description_
        """
        try:
            time_received = time.time_ns() // 1_000_000
            if id:
                game = self.get_game_by_id(id)
            else:
                user = self.get_user(request.query_params.get("username")) if request.query_params.get("username") else request.user
                
                game = get_active_game_of_user(user)
            if game is None:
                return CustomResponse(ErrorResponse.GAME_NOT_FOUND)
            game_data = ChessGameSerializer(game, time_with=False).data
            return CustomResponse(SuccessResponse.GAME_STATE_RESPONSE, data=game_data)

            # white_clock, black_clock, time_now = ChessClock.calculate_clocks(game, time_received=time_received)
            # return Response({'game' : chessgame_serializer.data,
            #                 'clock' : {'white_clock' : white_clock,
            #                             'black_clock' : black_clock,
            #                             'time_calculated_with' : time_now}
            #                 })
            # data = {'game' : chessgame_serializer.data,
            #                 'clock' : {'white_clock' : white_clock,
            #                             'black_clock' : black_clock,
            #                             'time_calculated_with' : time_now}
            #                 }
        except (Game.DoesNotExist, User.DoesNotExist, ValidationError):
            return CustomResponse(ErrorResponse.GAME_NOT_FOUND)
        
            

    def get_user(self, username):
        return User.objects.get(username=username)
    
    def get_game_by_id(self, id):
        return Game.objects.get(pk=id)

class NewGameApiView(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request, format=None):
        """
        Creates a new game with the currently logged in user

        Args:
            request (_type_): _description_
        """
        try:
            user = request.user
            if not user:
                return CustomResponse(ErrorResponse.NOT_ENOUGH_USER_INFO)
            colour = request.data.get("colour")
            if colour is not "w" and colour is not "b":
                return CustomResponse(ErrorResponse.INVALID_GAME_INFO)
            time = request.data.get("time")
            increment = request.data.get("increment")
        except KeyError as e:
            return CustomResponse(ErrorResponse.NOT_ENOUGH_GAME_INFO)
        if colour is None or time is None or increment is None:
            return CustomResponse(ErrorResponse.NOT_ENOUGH_GAME_INFO)
        existing_game = get_active_game_of_user(user)
        if existing_game:
            return CustomResponse(ErrorResponse.GAME_ALREADY_EXISTS)
        try:
            new_game = ChessGamesManager.create_new_game(user=user, colour=colour,
                                                        time=time, increment=increment)
        except TimeControl.DoesNotExist:
            return CustomResponse(ErrorResponse.INVALID_GAME_INFO)
        
        
        game_data = ChessGameSerializer(new_game, time_with=False).data
        return CustomResponse(SuccessResponse.GAME_CREATED, data=game_data)


class ResignApiView(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request, format=None):
        user = request.user
        game, colour = get_active_game_of_user_with_colour(user)
        if colour == "w":
            game.winner = "b"
        elif colour == "b":
            game.winner = "w"
        game.game_status = GameStatus.COMPLETED
        game.save()
        return CustomResponse(SuccessResponse.GAME_OVER)

class RegisterApiView(APIView):    
    def register_user(self, username, email, password):
        print("registering user {}".format(username))
        new_user = User.objects.create_user(username=username,
                                            email=email,
                                            password=password)
        new_user.save()
        return new_user
    
    def post(self, request, *args, **kwargs):
        username = request.data.get("username")
        email = request.data.get("email")
        password = request.data.get("password")
        if None in [username, email, password]:
            return CustomResponse(ErrorResponse.NOT_ENOUGH_USER_INFO)
        try:
            user = self.register_user(username, email, password)
        except IntegrityError:
            return CustomResponse(ErrorResponse.USER_EXISTS)
        print(UserSerializer(user).data)
        # return Response(data=UserSerializer(user).data, status=status.HTTP_201_CREATED)
        return CustomResponse(SuccessResponse.USER_CREATED)
        
        
class LoginApiView(APIView):
    def post(self, request, format=None):
        username = request.data.get("username")
        password = request.data.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return CustomResponse(SuccessResponse.LOGIN_SUCCESSFUL)
        else:
            return CustomResponse(ErrorResponse.LOGIN_FAILED)
            
# class ClocksApiView(APIView):
    
#     def get(self, request):
#         time_received = time.time_ns() // 1_000_000
#         games = Game.objects.all()
#         clocks_data = CalculatedChessClockSerializer(games, many=True).data
        
#         # return_data = {}
#         # for index, game in enumerate(games):
#         #     game_serialized = ChessGameSerializer(game)
#         #     white_clock, black_clock, time_now = ChessClock.calculate_clocks(game, time_received=time_received)
#         #     return_data[index] = {
#         #         'game_id' : game.game_id,
#         #         'white_clock' : white_clock,
#         #         'black_clock' : black_clock,
#         #         'time_calculated_with' : time_now
#         #     }
#         return Response(clocks_data, status=200)
    
# class ClockApiView(APIView):

#     def get(self, request, id=None, format=None):
#         game = None
#         try:
#             time_received=time.time_ns() // 1_000_000
#             if id:
#                 game = Game.objects.get(pk=id)
#             else:
#                 game = get_active_game_of_user(request.user)
#             if game is None:
#                 return CustomResponse(ErrorResponse.GAME_NOT_FOUND)
            
#             clock_data = CalculatedChessClockSerializer(game).data
#             # white_clock, black_clock, time_received = ChessClock.calculate_clocks(game, time_received=time_received)
#             # response = {
#             #     'white_clock' : white_clock,
#             #     'black_clock' : black_clock,
#             #     'time_calculated_with' : time_received
#             # }
#             return Response(clock_data, status=200)
#         except (User.DoesNotExist):
#             return CustomResponse(ErrorResponse.USER_NOT_FOUND)
#         except (Game.DoesNotExist, ValidationError):
#             return CustomResponse(ErrorResponse.GAME_NOT_FOUND)

# class GamesWithClocksApiView(APIView):
#     def get(self, request, format=None):
#         time_received = time.time_ns() // 1_000_000
#         games = Game.objects.all()
        
#         games_serialized = ChessGameSerializer(games, many=True).data
#         clocks_serialized = CalculatedChessClockSerializer(games, many=True).data
        
#         sorted_games = sorted(games_serialized, key=lambda d:d['game_id'])
#         sorted_clocks = sorted(clocks_serialized, key=lambda d:d['game_id'])

#         if len(sorted_games) != len(sorted_clocks):
#             raise HttpResponseServerError

#         merged_data = []
#         for index in range(len(sorted_games)):
#             print(str(sorted_games[index]["game_id"]))
#             print(str(sorted_clocks[index]["game_id"]))
#             if (str(sorted_games[index]["game_id"]) != str(sorted_clocks[index]["game_id"])):
#                 return HttpResponseServerError()
#             else: 
#                 merged_data.append({**sorted_games[index], **sorted_clocks[index]})
            
#         return Response(merged_data, status=200)
        
        
        

########### Moves ########### 

class ChessMoveApiView(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None): # TODO: look into anonymous sessioning for anon playing 
        active_game = get_active_game_of_user(request.user)
        if not active_game:
            return CustomResponse(ErrorResponse.GAME_NOT_FOUND)
        active_game_data = ChessGameSerializer(active_game, time_with=False).data

        # old_fen = request.GET("old_fen")
        # database_fen = active_game.current_fen
    
        
        # source = request.GET["source"]
        # target = request.GET["target"]
        move = request.data.get("move")
        if not move:
            return CustomResponse(ErrorResponse.INVALID_MOVE, data=active_game_data)
        try:
            new_game = ChessValidator.make_uci_move(active_game, move)
            if not new_game:
                return CustomResponse(ErrorResponse.INVALID_MOVE, data=active_game_data)
            game_data = ChessGameSerializer(new_game, time_with=False).data
            return CustomResponse(SuccessResponse.MOVE_MADE, data=game_data)
        except InvalidBoardError:
            return CustomResponse(ErrorResponse.INVALID_GAME_INFO, data=active_game_data)
            
        
        
    
class ValidMoveApiView(APIView):
    def get(self, request, format=None):
        fen = request.query_params.get('fen') if request.query_params.get('fen') else None
        move = request.query_params.get('move') if request.query_params.get('move') else None

        if (fen is None) or (move is None):
            return CustomResponse(ErrorResponse.NOT_ENOUGH_GAME_INFO)
        
        try:
            is_valid = ChessValidator.is_valid_move(fen=fen, move=move)
        except (InvalidBoardError) as e:
            return CustomResponse(ErrorResponse.INVALID_FEN)
        except (InvalidMoveError):
            return CustomResponse(ErrorResponse.INVALID_MOVE)
        
        if is_valid:
            return CustomResponse(SuccessResponse.VALID_MOVE)
        else:
            return CustomResponse(ErrorResponse.INVALID_MOVE) # still success response as nothing went "wrong".

class ValidPgnApiView(APIView):
    def get(self, request, format=None):
        pgn = request.query_params.get('pgn') if request.query_params.get('pgn') else None
        
        if pgn is None:
            return CustomResponse(ErrorResponse.NOT_ENOUGH_INFORMATION)
        
        is_valid = ChessValidator.is_valid_pgn(pgn_str=pgn)
        return CustomResponse(SuccessResponse.VALID)
        
class ValidFenApiView(APIView):
    def get(self, request, format=None):
        fen = request.query_params.get('fen') if request.query_params.get('fen') else None
        
        if not fen:
            return CustomResponse(ErrorResponse.NOT_ENOUGH_INFORMATION)
        
        is_valid = ChessValidator.is_valid_fen(fen)
        return CustomResponse(SuccessResponse.VALID)
    
class LegalMovesApiView(APIView):
    def get(self, request, id=None, format=None):
        try:
            if id:
                game = Game.objects.get(pk=id)
                fen = game.current_fen
            else:
                fen = request.query_params.get('fen') if request.query_params.get('fen') else None
            
            if not fen:
                return CustomResponse(ErrorResponse.NOT_ENOUGH_INFORMATION)
            
            legal_moves = ChessValidator.get_legal_moves(fen)
            response = {}
            for index, move in enumerate(legal_moves):
                response[index] = str(move)
            return Response(response, status=200)
                
        except (Game.DoesNotExist):
            return CustomResponse(ErrorResponse.GAME_NOT_FOUND)
        except InvalidBoardError as e:
            return CustomResponse(ErrorResponse.INVALID_INFORMATION, details=str(e))
            
            
        

