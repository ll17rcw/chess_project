from django.test import TestCase, Client
from chess_site.models import Game
from chess_site.models import User, Game
from rest_framework import status

from chess_site.responses import ErrorResponse, SuccessResponse
from chess_site.serializers import UserSerializer


class RegisterUserTest(TestCase):
    
    def setUp(self):
        self.client = Client()
    
    def test_register_new_user(self):
        username = 'user1'
        email = "test@test.com"
        password = 'password'
        response = self.client.post("/register/", {
            'username' : username,
            'email' : email,
            'password' : password
        })
        
        user_exists = User.objects.filter(username=username).exists()

        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(user_exists)
        
    def test_reregister_user_all(self):
        username = 'user1'
        email = "test@test.com"
        password = 'password'
        
        # expected_user = User.objects.create_user(username=username, email=email, password=password)
        user_exists = User.objects.filter(username=username).exists()
        
        self.assertFalse(user_exists) # Ensure user is not added before post call.
        
        response1 = self.client.post("/register/", {
            'username' : username,
            'email' : email,
            'password' : password
        })
        
        user_exists = User.objects.filter(username=username).exists()
        expected_status_code = status.HTTP_201_CREATED
        expected_response_data = UserSerializer(User(username=username,
                                                email=email,
                                                password=password)).data
        
        self.assertEqual(response1.status_code, expected_status_code)
        self.assertEqual(response1.data, expected_response_data)
        self.assertTrue(user_exists) # Ensure user exists in database
        
        
        response2 = self.client.post("/register/", {
            'username' : username,
            'email' : email,
            'password' : password
        })
        
        self.assertEqual(response2.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue(user_exists)
    
    def test_reregister_user_username(self):
        pass # Not implemented

    def test_reregister_user_email(self):
        pass # Not implemented
    
    def test_register_missing_parameters(self):
        username = "user1"
        email = "user1@email.com"
        password = "password"
        
        user_exists = User.objects.filter(username=username).exists()
        
        self.assertFalse(user_exists) # Ensure user is not added before post call.

        
        response = self.client.post("/register/", {
            'username' : username,
            'email' : email,
            'password' : password
        })
        
        user_exists = User.objects.filter(username=username).exists()
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = ErrorResponse.NOT_ENOUGH_INFORMATION
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
        
        
        
        
        
        
        