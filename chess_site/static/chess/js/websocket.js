let socket = null;

function open(){
    sendMessage("CONNECT", "");
}

function onMessage(){}

function connect(connStr) {
    console.log("CONNECTING...")
    socket = new WebSocket(connStr);

    socket.addEventListener('open', function (event) {
        // console.log("Open function ran");
        sendMessage("CONNECT", "");
    });

    socket.addEventListener('message', function (e) {
        // On getting the message from the server
        // Do the appropriate steps on each event.
        let data = JSON.parse(e.data);
        data = data["payload"];
        let message = data['message'];
        let event = data["event"];
        let message_info = data["message_info"]
    
        switch (event) {
            case "CONNECT":
                console.log("opponent connected")
                break;
            case "DISCONNECT":
                console.log("opponent disconnected")
                break;
            case "START":
                startGame
                break;
            case "END":
                alert(message);
                // reset();
                break;
            case "MOVE":
                // console.log("message: ", message["oldPos"]);
                console.log("recevied move")
                madeMove(message["oldPos"], message["newPos"], message["source"], message["target"])
                // console.log("newPos", message["newPos"]);
                break;
            case "INIT_BOARD":
                fen = message["fen"]
                // pgn_headers = message["pgn_headers"]
                // pgn_moves = message["pgn_moves"] 
                pgn = message["pgn"]
                pgn_headers = message["pgn_headers"]
                time = message["time"]
                increment = message["increment"]
                console.log("time recevied: " + time)
                console.log("increment received: " + increment)


                setClock(time, increment)
                setBoard(fen, pgn, pgn_headers)
                
                // setMoveList(pgn)
                break;
            case "INVALID_MOVE":
                // revertMove(message["oldFen"]);
                break;
            case "GAME_OVER":
                console.log("GAME OVER")
                alert("Game over\n\n" + message["outcome"])
                
            case "TEST":
                console.log("message recieved: " + message)
                break;
            default:
                console.log("No such event: " + event)
        }
    });

    socket.addEventListener('close', function (e) {
        console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
        setTimeout(function () {
            connect(connStr);
        }, 1000);
    });
  }

function sendMessage(event, message, player){
/**
 * Sends a message 
 */
    console.log("Send message ran event: " + event + " message: " + message);
    if (socket === null){
        console.error("Error: Game Socket is null");
        return;
    }

    let data = {
        "event" : event,
        "message" : message
        // "player" : player //is this needed?
    }

    socket.send(JSON.stringify(data))
}


function attemptConnect(connStr){
    endPoint = "/api/game"
    // console.log("making request to " + endPoint)
    connected = false
    $.ajax({
        type : 'GET',
        url : endPoint,
        dataType : 'json',
        success : function(res){
            console.log("SUCCESS")
            // console.log(res)
            connect(connStr);
        },
        error: function(res){
            console.log("Error connecting to server")
            
        }
    })
    // console.log("is socket null? " + socket==null)
    if (socket == null){
        setTimeout(function(){
            attemptConnect(connStr)
        }, 5000)
    }
    
}
