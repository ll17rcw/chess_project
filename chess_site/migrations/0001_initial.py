# Generated by Django 3.2.8 on 2022-03-24 11:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='TimeControl',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.IntegerField(default=1)),
                ('increment', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='GameRecord',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('player_b', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gamerecord_player_black', to=settings.AUTH_USER_MODEL)),
                ('player_w', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='gamerecord_player_white', to=settings.AUTH_USER_MODEL)),
                ('time_control', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='chess_site.timecontrol')),
            ],
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('game_id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('current_fen', models.CharField(default='rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR', max_length=2056)),
                ('datetime_started', models.DateTimeField(default=django.utils.timezone.now)),
                ('game_status', models.IntegerField(choices=[(0, 'Pending'), (1, 'Ongoing'), (2, 'Completed')], default=0)),
                ('game_time', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='games', to='chess_site.timecontrol')),
                ('player_b', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='game_player_black', to=settings.AUTH_USER_MODEL)),
                ('player_w', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='game_player_white', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
