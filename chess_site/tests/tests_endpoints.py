from typing import Dict, List
from django.test import TestCase, Client
from abc import ABC, abstractmethod
from itsdangerous import Serializer
from rest_framework import status
import sys
from chess_site.data.data import SITE_DATA

from chess_site.responses import CustomResponse
from chess_site.urls_generator import UrlsGenerator


# class TestApiEndpoint(TestCase):

#     def setUp(self):
#         self.client = Client()
    
#     @property
#     @abstractmethod
#     def endpoint(self) -> str:
#         pass

#     @property
#     @abstractmethod
#     def expected_response_id(self) -> CustomResponse:
#         pass
    
#     @property
#     @abstractmethod
#     def expected_response_content(self) -> Serializer:
#         pass
    
#     @property
#     @abstractmethod
#     def expected_status_code(self) -> status:
#         pass
    
#     @property
#     @abstractmethod
#     def method(self) -> str:
#         pass
    
#     @property
#     @abstractmethod
#     def parameters(self) -> Dict:
#         pass
    
        
#     def test_endpoint(self):
#         """
#         Tests an endpoint against expected responses.
#         Tests for both expected response ID and expected status code
#         for GET and POST requests with given parameters (if any).
#         """
#         if self.method == "GET":
#             response = self.client.get(self.endpoint, data=self.parameters)
#         elif self.method == "POST":
#             response = self.client.post(self.endpoint, data=self.parameters)
#         else:
#             print("Method '{}' not accepted".format(self.method))
#             sys.exit()
            
#         self.assertEqual(response.status_code, self.expected_status_code)
#         self.assertEqual(response.data["id"], self.expected_response_id)
#         self.assertEqual(response.data["content"], self.expected_response_content)
        
# class TestGamesEndpoint(TestApiEndpoint):
#     endpoint = "games/"
#     expected_response_id = "GAMES"
#     expected_response_content = ""
#     expected_status_code = status.HTTP_200_OK
#     method = "GET"
#     parameters = None
    

    
class TestEndpoints(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_endpoints(self):
        endpoints = UrlsGenerator.get_all_endpoints()
        for endpoint in endpoints:
            response = self.client.get(endpoint)
            self.assertNotEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        
        
    
        