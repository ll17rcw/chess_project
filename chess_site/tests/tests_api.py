"""
Tests all the API endpoints and their responses.
In all cases, tests are conducted under "perfect" conditions for their expected outcome
and are created / inserted with the ideal data.
Each TestCase represents a seperate endpoint of the API with each test within representing each case.
"""


from django.test import TestCase, Client
from rest_framework import status
from rest_framework.response import Response
from chess_site.chessclock import ChessClock
from chess_site.models import User, Game, TimeControl
from chess_site.responses import CustomResponse, ErrorResponse, ResponseEnum, SuccessResponse
from chess_site.serializers import ChessClockSerializer, ChessGameSerializer, CalculatedChessClockSerializer
import time
import random


def validate_custom_response(response : Response, expected_response_enum : ResponseEnum):
    expected_response = CustomResponse(expected_response_enum)
    expected_response_status_code = expected_response.status_code
    expected_response_id = expected_response.id
    
    return ((response.status_code == expected_response_status_code) and 
            (response.data["response_id"]), expected_response_id)
    
    
    

        

class ApiGameTest(TestCase):
    """
    Test cases to check the api game endpoints are receiving requests and sending data.
    """
    def setUp(self):
        self.client = Client()
        self.user1 = User.objects.create(username="user1", password="user1")
        self.user2 = User.objects.create(username="user2", password="user2")
        self.user3 = User.objects.create(username="user3", password="user3")
        self.user4 = User.objects.create(username="user4", password="user4")


        self.test_time_control = TimeControl.objects.create()
        
        self.game1 = Game.objects.create(
            game_time = self.test_time_control,
            player_w = self.user1,
            player_b = self.user2,
        )
        
        
        self.game2 = Game.objects.create(
            game_time = self.test_time_control,
            player_w = self.user3,
            player_b = self.user4
        )

    
    def test_get_all_games(self):
        """
        Tests the endpoint /api/games/ for all games.
        """
        response = self.client.get("/api/games/", follow=True) # TODO: can this be made so it is same as documentation?
        all_games = Game.objects.all()
        expected_response = ChessGameSerializer(all_games, many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, expected_response.data)
    
    def test_get_single_game(self):
        """
        Tests the endpoint for /api/game/ for single game.
        """
        response = self.client.get("/api/game/{}".format(self.game1.game_id), follow=True)
        expected_response = ChessGameSerializer(Game.objects.get(pk=self.game1.game_id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, expected_response.data)
        
            
        

class ApiRandomGameTest(TestCase):
    """
    Test cases to check the api game endpoints are receiving requests and sending data.
    """
    def setUp(self):
        self.client = Client()
        
        self.min_num_time_controls = 1
        self.max_num_time_controls = 1000
        
        self.min_num_users = 2
        self.max_num_users = 1000
        
        self.min_num_games = 1
        self.max_num_games = 1000
        
        self.num_time_control = random.randint(self.min_num_time_controls, self.max_num_time_controls)
        self.num_users = random.randint(self.min_num_users, self.max_num_time_controls)
        self.num_games = random.randint(self.min_num_games, self.max_num_games)
        
        self.time_controls = []
        self.users = []
        self.games = []
        for time_control in range(self.num_time_control):
            new_time_control = TimeControl.objects.create(
                time = time_control,
                increment= time_control
            )
            self.time_controls.append(new_time_control)
        
        for user_num in range(self.num_users):
            username = "user{}".format(user_num)
            password = "password{}".format(user_num)
            
            new_user = User.objects.create(username=username,
                                           password=password)
            self.users.append(new_user)
        
        for game_num in range(self.num_games):
            user_ids = random.sample(range(self.min_num_users, self.num_users), 2)
            time_control_id = random.randint(self.min_num_time_controls, self.num_time_control)
            user1 = User.objects.get(pk=user_ids[0])
            user2 = User.objects.get(pk=user_ids[1])
            time_control = TimeControl.objects.get(pk=time_control_id)


            new_game = Game.objects.create(
                game_time = time_control,
                player_w = user1,
                player_b = user2
            )
            self.games.append(new_game)

    
    def test_get_all_games(self):
        """
        Tests the endpoint /api/games/ for all games.
        """
        response = self.client.get("/api/games/", follow=True) # TODO: can this be made so it is same as documentation?
        all_games = Game.objects.all()
        expected_response = ChessGameSerializer(all_games, many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, expected_response.data)
    
    def test_get_single_game(self):
        """
        Tests the endpoint for /api/game/ for all games one at a time.
        """
        for game in self.games:
            response = self.client.get("/api/game/{}".format(game.game_id), follow=True)
            expected_response = ChessGameSerializer(Game.objects.get(pk=game.game_id))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data, expected_response.data)
            
class ApiClockTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user1 = User.objects.create(username="user1", password="user1")
        self.user2 = User.objects.create(username="user2", password="user2")
        self.user3 = User.objects.create(username="user3", password="user3")
        self.user4 = User.objects.create(username="user4", password="user4")


        self.test_time_control = TimeControl.objects.create()
        
        self.game1 = Game.objects.create(
            game_time = self.test_time_control,
            player_w = self.user1,
            player_b = self.user2,
        )
        
        
        self.game2 = Game.objects.create(
            game_time = self.test_time_control,
            player_w = self.user3,
            player_b = self.user4
        )
        
    def test_get_all_clocks(self):
        request_made_time = time.time_ns() // 1_000_000
        response = self.client.get("/api/clocks/", follow=True)
        games = Game.objects.all()
        expected_response = CalculatedChessClockSerializer(games, many=True)
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, expected_response.data)
    
    def test_get_single_clock(self):
        request_made_time = time.time_ns() // 1_000_000
        response = self.client.get("/api/clock/{}".format(self.game1.game_id), follow=True)
        game = Game.objects.get(pk=self.game1.game_id)
        expected_response = CalculatedChessClockSerializer(game, many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, expected_response.data)

class ApiRandomClockTest(TestCase):
    """
    Test cases to check the api game endpoints are receiving requests and sending data.
    """
    def setUp(self):
        self.client = Client()
        
        self.min_num_time_controls = 1
        self.max_num_time_controls = 1000
        
        self.min_num_users = 2
        self.max_num_users = 1000
        
        self.min_num_games = 1
        self.max_num_games = 1000
        
        self.num_time_control = random.randint(self.min_num_time_controls, self.max_num_time_controls)
        self.num_users = random.randint(self.min_num_users, self.max_num_time_controls)
        self.num_games = random.randint(self.min_num_games, self.max_num_games)
        
        self.time_controls = []
        self.users = []
        self.games = []
        for time_control in range(self.num_time_control):
            new_time_control = TimeControl.objects.create(
                time = time_control,
                increment= time_control
            )
            self.time_controls.append(new_time_control)
        
        for user_num in range(self.num_users):
            username = "user{}".format(user_num)
            password = "password{}".format(user_num)
            
            new_user = User.objects.create(username=username,
                                           password=password)
            self.users.append(new_user)
        
        for game_num in range(self.num_games):
            user_ids = random.sample(range(self.min_num_users, self.num_users), 2)
            time_control_id = random.randint(self.min_num_time_controls, self.num_time_control)
            user1 = User.objects.get(pk=user_ids[0])
            user2 = User.objects.get(pk=user_ids[1])
            time_control = TimeControl.objects.get(pk=time_control_id)


            new_game = Game.objects.create(
                game_time = time_control,
                player_w = user1,
                player_b = user2
            )
            self.games.append(new_game)

    def test_get_all_clocks(self):
        response = self.client.get("/api/clocks/", follow=True)
        all_games = Game.objects.all()
        expected_response = CalculatedChessClockSerializer(all_games, many=True)
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, expected_response.data)
    
    def test_get_single_clock(self):
        for game in self.games:
            response = self.client.get("/api/clock/{}".format(game.game_id), follow=True)
            expected_response = CalculatedChessClockSerializer(game)
            
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data, expected_response.data)




class ApiValidMoveTest(TestCase):
    
    def setUp(self):
        self.client = Client()
        self.valid_fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
        self.invalid_fen = "rnbqkpnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" # pawn on the backrank
        self.unparsable_fen = "this is an unparsable fen"
        self.valid_move = "e2e3"
        self.invalid_move = "e6e7" # TODO: any way to guarantee that these are valid / invalid?
        self.unparsable_move = "This is an unparsable move"
    
        
        self.validmove_endpoint = "/api/validmove/"
        
    def test_valid_move(self):
        response = self.client.get(self.validmove_endpoint, {
            "fen" : self.valid_fen,
            "move" : self.valid_move
        })
        
        self.assertTrue(validate_custom_response(response, SuccessResponse.VALID_MOVE))

        
    def test_invalid_move(self):
        response = self.client.get(self.validmove_endpoint,{
            "fen" : self.valid_fen,
            "move" : self.invalid_move
        })
        
        self.assertTrue(validate_custom_response(response, SuccessResponse.INVALID_MOVE))

    def test_invalid_fen_valid_move(self):
        response = self.client.get(self.validmove_endpoint, {
            "fen" : self.invalid_fen,
            "move" : self.valid_move
        })
        
        self.assertTrue(validate_custom_response(response, ErrorResponse.INVALID_INFORMATION))
        
    def test_invalid_fen_invalid_move(self):
        response = self.client.get(self.validmove_endpoint, {
            "fen" : self.invalid_fen,
            "move" : self.invalid_move
        })
        
        self.assertTrue(validate_custom_response(response, ErrorResponse.INVALID_INFORMATION))
    
    def test_unparsable_fen(self):
        response = self.client.get(self.validmove_endpoint, {
            "fen" : self.unparsable_fen,
            "move" : self.valid_move
        })
        
        self.assertTrue(validate_custom_response(response, ErrorResponse.INVALID_INFORMATION))
        
    def test_unparsable_move(self):
        response = self.client.get(self.validmove_endpoint, {
            "fen" : self.valid_fen,
            "move" : self.unparsable_move
        })
        
        self.assertTrue(validate_custom_response(response, ErrorResponse.INVALID_INFORMATION))
        
    def test_no_fen(self):
        response = self.client.get(self.validmove_endpoint, {
            "move" : self.unparsable_move
        })
        
        self.assertTrue(validate_custom_response(response, ErrorResponse.NOT_ENOUGH_INFORMATION))
        
    def test_no_move(self):
        response = self.client.get(self.validmove_endpoint, {
            "fen" : self.valid_fen
        })
        
        self.assertTrue(validate_custom_response(response, ErrorResponse.NOT_ENOUGH_INFORMATION))
        
    def test_no_information(self):
        response = self.client.get(self.validmove_endpoint)
        
        self.assertTrue(validate_custom_response(response, ErrorResponse.NOT_ENOUGH_INFORMATION))
        
    


        

    
        
        
        



        