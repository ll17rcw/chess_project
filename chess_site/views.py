from django.http import HttpResponseBadRequest
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.db.models import Q
import django.core.exceptions
from django.db import DatabaseError, transaction
import redis
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

from .forms import UserRegisterForm, GameCreationForm
from .models import Game, GameStatus, TimeControl
from .serializers import ChessGameSerializer
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.db.utils import IntegrityError
from rest_framework import  status
from rest_framework.response import Response
from rest_framework.views import APIView
from chess_site.serializers import UserSerializer, ChessGameSerializer


# Create your views here.

from django.contrib.auth.decorators import login_required


def home(request):    
    return render(request, "chess/home.html")

def chess(request):
    return render(request, 'chess/home.html')

def chat(request):
    return render(request, 'chess/chat.html')

def room(request, room_name):
    return render(request, 'chess/room.html', {
        'room_name' : room_name
    })
    

# def login(request):
#     username = request

@login_required
def game(request, game_id):
    print("game_id: " + str(game_id))
    
    current_user = request.user
    game = Game.objects.get(game_id=game_id)
    
    
    if (not game):
        return HttpResponse("Error: no game exists with id: " + str(game_id)) # TODO: change these reponse to something nicer

    player_w = game.player_w
    player_b = game.player_b
    
    if (current_user == player_w):
        colour = "w"
    elif (current_user == player_b):
        colour = "b"
    else:
        return HttpResponse("You do not belong to this game.")

    if colour not in ['w', 'b']:
        raise HttpResponse("Colour must be white or black")
    
    context = {
        "game_id" : game_id,
        "colour" : colour
    }
    
    return render(request, 'chess/game.html', context)


def get_game_time(game_time):
    print(game_time)
    time, increment = game_time.split("+", 2)
    game_time_object = TimeControl.objects.get(time=time, increment=increment)
    return game_time_object

# def generate_gameid():

def get_active_game_of_user(user):
    """
    Gets the game of the user or null

    Args:
        user (models.User): User to get the game of
        
    Returns:
        game (models.Game) : active game that user belongs to or None
        
    """
    if (user is None):
        return None
    existing_game = Game.objects.select_for_update().filter((Q(player_w = user) | Q(player_b = user)) & ~Q(game_status = GameStatus.COMPLETED))
    if (len(existing_game) > 1):
        raise Exception("Error: User {} has multiple active (non-completed) games.".format())
    if (not existing_game):
        return None

    return existing_game[0]
    
def get_active_game_of_user_with_status(user):
    """
    Gets the game of the user and the status or null

    Args:
        user (models.User): User to get the game of
        
    Returns:
        existing_game (models.Game) : active game that user belongs to or None
        game_status (models.GameStatus) : status of the game
        
    """
    existing_game = Game.objects.filter((Q(player_w = user) | Q(player_b = user)) & ~Q(game_status = GameStatus.COMPLETED))
    if (len(existing_game) > 1):
        raise Exception("Error: User {} has multiple active (non-completed) games.".format())
    if (not existing_game):
        return None, None

    existing_game = existing_game[0]
    return existing_game, existing_game.game_status

def get_active_game_of_user_with_colour(user):
    """
    Gets the game and colour of the user or null

    Args:
        user (models.User): User to get the game (and colour) of

    Returns:
        game (models.Game), colour (str) : active game and colour
    """
    game = Game.objects.filter((Q(player_w = user) | Q(player_b = user)) & ~Q(game_status = GameStatus.COMPLETED))

    if (not game):
        return None, None
    
    game = game[0]
    
    if (game.player_w == user):
        return game, "w"
    if (game.player_b == user):
        return game, "b"

    
def find_game(request):
 
    # Game.objects.filter((Q(player_w = user) | Q(player_b = user)) & ~Q(game_status = GameStatus.COMPLETED))[0]
    if (request.is_ajax()):
        if (request.method == "GET"):
            existing_games = Game.objects.all().filter(game_status=GameStatus.FORMED)
            
            # return JsonResponse({"games" : ChessGameSerializer(existing_games, many=True).data})
            context = {'games' : existing_games}
            return render(request, "chess/findgame_game.html", context)
        elif (request.method == "POST"):
            game_id = request.POST.get("game_id")
            print("game_id: " + str(game_id))
            with transaction.atomic():
                try:
                    game = Game.objects.select_for_update(nowait=True).get(game_id=game_id)
                    if (game.game_status != GameStatus.FORMED):
                        return HttpResponseBadRequest("Game already under way!")
                    if (game.player_w):
                        game.player_b = request.user
                    elif (game.player_b):
                        game.player_w = request.user
                    game.game_status = GameStatus.PENDING
                    game.save()
                    print("made it to the end")
                except Game.DoesNotExist:
                    return HttpResponse(status=404)
                except DatabaseError:
                    return HttpResponse(status=500)
                
            return HttpResponse(200)


    return render(request, "chess/findgame.html")
    
@login_required
def search(request):
    """
    If user has an existing game, look for opponent for that game.

    Args:
        request (_type_): _description_

    Returns:
        _type_: _description_
    """
    existing_game, game_status = get_active_game_of_user_with_status(request.user)
    if (request.is_ajax()):
        if (existing_game):
            return JsonResponse(ChessGameSerializer(existing_game, time_with=False).data)
        else:
            existing_games = Game.objects.all().filter(game_status=GameStatus.FORMED)
            if not existing_games:
                return JsonResponse({})
            print(ChessGameSerializer(existing_games, many=True).data)
            return JsonResponse({"games" : ChessGameSerializer(existing_games, many=True, time_with=False).data})
            
            

    return render(request, 'chess/search.html')
   
   
def test(request):
    print(str(Game.objects.get(pk='3aa144a63f6e4d4aa4188e06bb229d7f')))

 
@login_required
def play(request):
    print("IN PLAY")
    
    existing_game, game_status = get_active_game_of_user_with_status(request.user)
    if (existing_game):    
        
        if (request.is_ajax()):
            return JsonResponse({"fen" : existing_game.current_fen})
        
        game_id = str(existing_game.game_id).replace("-", "")
        
        if game_status == GameStatus.FORMED:
            return redirect('/search/')
        elif (game_status == GameStatus.PENDING) or (game_status == GameStatus.ONGOING):
            return redirect(
                    '/play/%s'
                    %(game_id))
    return render(request, 'chess/play.html')

def create_new_game(game_time, white_clock, black_clock, player_w = None, player_b = None):
    if (get_active_game_of_user(player_w) or get_active_game_of_user(player_b)):
        raise django.core.exceptions.BadRequest("User already has active game when attempting to create new game")
    
    
    new_game = Game(game_time = game_time,
                    player_w = player_w,
                    player_b = player_b,
                    white_clock = white_clock,
                    black_clock = black_clock)
    
    new_game.save()
    return new_game

def minutes_to_milliseconds(minutes):
    return minutes * 60000
    

@login_required
def create_game(request):
    r = redis.Redis.from_url('redis://localhost')
    print(r)
    prefix = 'asgi::group:'
    groups = [key.decode('utf-8')[len(prefix):] for key in r.scan_iter(prefix + '*')]
    print(groups)
    
    
    game_creation_form = GameCreationForm(request.POST)

    existing_game, game_status = get_active_game_of_user_with_status(request.user)
    if (existing_game):
        
        game_id = str(existing_game.game_id).replace("-", "")
        
        if (game_status == GameStatus.FORMED):
            return redirect('/search/')
        elif ((game_status == GameStatus.PENDING) or (game_status == GameStatus.ONGOING)):
            return redirect(
                    '/play/%s'
                    %(game_id))
            
    if (request.method == 'POST'):
        game_creation_form = GameCreationForm(request.POST)
        if (game_creation_form.is_valid()):
            
            time_control_dict = dict(game_creation_form.fields['time_control'].choices)
            time_control = time_control_dict.get(int(game_creation_form.cleaned_data.get('time_control')))
            colour = game_creation_form.cleaned_data.get('colour')
            game_time = get_game_time(time_control)
            player_w = None
            player_b = None
            if (colour == "w"): # TODO: can use enums 
                player_w = request.user
            elif (colour == "b"):
                player_b = request.user
                
            new_game = create_new_game(game_time=game_time,
                            white_clock = minutes_to_milliseconds(game_time.time),
                            black_clock = minutes_to_milliseconds(game_time.time),
                            player_w = player_w,
                            player_b = player_b)
            

            
            game_id = str(new_game.game_id).replace("-", "") # TODO: any way to get without hashes without doing this?
            
            return redirect('/search/')

    
    return render(request, 'chess/newgame.html', {'form' : game_creation_form})
    


# def register(request):
#     if (request.user.is_authenticated):
#         return redirect("home")
        
#     if request.method == 'POST':
#         user_creation_form = UserRegisterForm(request.POST)
#         if (user_creation_form.is_valid()):
#             user_creation_form.save()
#             username = user_creation_form.cleaned_data.get('username')
#             messages.success(request, f'Account created for {username}')
#             return redirect('chess-home')
            
#     else:
#         user_creation_form = UserRegisterForm()
    
#     return render(request, 'account/register.html', {'form' : user_creation_form})

@method_decorator(csrf_exempt, name='dispatch')
class RegisterView(APIView):
    form_class = UserRegisterForm
    template = 'account/register.html'
    
    def register_user(self, username, email, password):
        print("registering user {}".format(username))
        new_user = User.objects.create_user(username=username,
                                            email=email,
                                            password=password)
        new_user.save()
        return new_user
        

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template, {'form' : form})
    
    def post(self, request, *args, **kwargs):
        form = self.form_class()
        if form.is_valid():
            user = self.register_user(form["username"], form["email"], form["password"])
            return redirect('/')
        else:
            print("ERRORS: ")
            print(form.errors)
        username = request.POST["username"]
        email = request.POST["email"]
        password = request.POST["password1"]
        try:
            user = self.register_user(username, email, password)
        except IntegrityError:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        print(UserSerializer(user).data)
        return Response(data=UserSerializer(user).data, status=status.HTTP_201_CREATED)

    def form_valid(self, form):
        if form.is_valid(): # if the form is submitted
            form.save()
            username = form.cleaned_data.get('username')
            return super().form_valid(form)
        
        
        
    
            
                    
        
    

    
# class login_page(View):
#     def get(self, request):
#         return render(request, 'account/login.html')
    
#     def post(self, request):
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(request, username=username, password=password)
#         if user is not None:
#             login(request, user)
#             messages.add_message("Login Successful")
#             return HttpResponseRedirect('/chess/')
#         else:
#             return HttpResponseRedirect(reverse("register"))
            
# class register(View):
#     def get(self, request):
#         return render(request, "account/register.html")
    
#     def post(self, request):
#         first_name = request.POST["first_name"]
#         surname = request.POST["surname"]
#         username = request.POST["username"]
#         password = request.POST["password"]
        
#         new_user = User.objects.create_user()
        
        
        

#######
# API #
#######


# @api_view(['GET'])
# @authentication_classes
# def chessgame(request):
#     """
#     Get active chess game of player else null
#     """
#     if request.method == 'GET':
#         game = Game.objects.filter()

    # permission_classes = [permissions.IsAuthenticated]

# class GameViewSet(viewsets.ReadOnlyModelViewSet):
#     """
#     API endpoint that returns the a game

#     Args:
#         viewsets (_type_): _description_
#     """
#     serializer_class = ChessGameSerializer
    
#     def get_queryset(self):
#         user = self.request.user
#         existing_game = Game.objects.filter((Q(player_w = user) | Q(player_b = user)) & ~Q(game_status = GameStatus.COMPLETED))
#         return existing_game

# class GameApiView(APIView):
#     def get(self, request, format=None):
#         print("gameapiview here")
#         user = request.user
#         existing_game = Game.objects.filter((Q(player_w = user) | Q(player_b = user)) & ~Q(game_status = GameStatus.COMPLETED))[0]
#         # return JsonResponse({'id' : existing_game.game_id})
#         # return JsonResponse({'game' :ChessGameSerializer(existing_game).data,
#         #                      'clock' : ChessClockSerializer(existing_game).data})
    
    
# class MatchmakingApiView(APIView):
#     def get(self, request, format=None):
#         if not request.user.is_authenticated:
#             return HttpResponseBadRequest
#         user = request.user
#         existing_game = get_active_game_of_user(user)
#         if existing_game.game_status == GameStatus.ONGOING:
#             result = {'status' : 'ONGOING'}
#             return JsonResponse(result)
#         matchmaking_instance = MatchmakingInstance.objects.get(user=user)
#         if matchmaking_instance.matchmaking_status == MatchmakingStatus.COMPLETED:
#             result = {'status' : 'COMPLETED'}
#             return JsonResponse(result)
#         else:
#             result = {'status' : 'PENDING'}
#             return JsonResponse(result)
        

# class GameApiView(APIView):
#     authentication_classes = [SessionAuthentication, BasicAuthentication]
#     permission_classes = [IsAuthenticated]

#     @transaction.atomic
#     def get(self, request, format=None):
#         game = get_active_game_of_user(request.user)
#         game = ChessClock.calculate_and_update_clock(game)
#         game.save()

#         chessgame_serializer = ChessGameSerializer(game)
#         chessclock_serializer = ChessClockSerializer(game)
#         return JsonResponse({'game' : chessgame_serializer.data,
#                              'clock' : chessclock_serializer.data}, safe=False)
    
# class ValidMoveApiView(APIView):
#     def get(self, request, format=None):
#         active_game = get_active_game_of_user(request.user)
#         if not active_game:
#             print("not active game")
#             return HttpResponse(status=404)
#         database_fen = active_game.current_fen
#         old_fen = request.GET["old_fen"]
        
#         if (database_fen != old_fen):
#             print("database fen doesnt match")
#             print("database_fen: {} \n\n", database_fen)
#             print("old_fen : {}", old_fen)
#             return JsonResponse({"error" : "FEN_UNMATCHED"}, status=400)
            
#         source = request.GET["source"]
#         target = request.GET["target"]
        
#         if ChessValidator.is_valid_move(old_fen, source, target):
#             return JsonResponse({"valid_move" : True})
#         else:
#             return JsonResponse({"valid_move" : False})
        
# class ChessMoveApiView(APIView):
#     def post(self, request, format=None):
#         active_game = get_active_game_of_user(request.user)
#         if not active_game:
#             return HttpResponse(status=404)

#         old_fen = request.GET("old_fen")
#         database_fen = active_game.current_fen
        
#         if (database_fen != old_fen):
#             print("database fen doesnt match.")
#             return JsonResponse({"error" : "FEN_UNMATCHED"}, status=400)
        
#         source = request.GET["source"]
#         target = request.GET["target"]
        
#         if (not ChessValidator.is_valid_move(old_fen, source, target)):
#             return JsonResponse({"error" : "MOVE_NOT_VALID"}, status=400)
        
#         white_clock = request.GET["white_clock"]
#         black_clock = request.GET["black_clock"]
        
            
        
        
        
        
        

    
    

    
