from django.urls import path, include
from chess_site import api as api
# # from chess_site.urls.urls_generator import UrlsGenerator
from chess_site.urls_generator import UrlsGenerator
  
# urlpatterns = []
# for endpoint, function_name in UrlsGenerator.construct_urlpatterns("api"):
#     # urlpatterns = path(UrlsGenerator.construct_urlpatterns())
#     function = getattr(api, function_name).as_view()
#     urlpatterns.append(path(endpoint, function))

urlpatterns = UrlsGenerator.construct_urlpatterns_r(api)

# urlpatterns = [  
#     path('games/', api.GamesApiView.as_view()),
#     path('game/', api.GameApiView.as_view()),
#     path('game/<id>', api.GameApiView.as_view()),
#     path('clocks/', api.ClocksApiView.as_view()),
#     path('clock/', api.ClockApiView.as_view()),
#     path('clock/<id>', api.ClockApiView.as_view()),
#     path('gameswithclocks/', api.GamesWithClocksApiView.as_view()),
#     path('validmove/', api.ValidMoveApiView.as_view()),
#     path('validpgn/', api.ValidPgnApiView.as_view()),
#     path('validfen/', api.ValidPgnApiView.as_view()),
#     path('legalmoves/', api.LegalMovesApiView.as_view()),
#     path('legalmoves/<id>', api.LegalMovesApiView.as_view()),

# ]
    