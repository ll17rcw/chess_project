
$(function () {
    $.ajaxSetup({
        headers: { "X-CSRFToken": $.cookie("csrftoken") }
    });
});


function joinGame(game_id){
    console.log("game id : " + game_id)
    $.post("/findgame/", {"game_id" : game_id}, function(data, status){
        alert("status: " + status + "\ndata : " + data)
    })
}


function findGame(){
    console.log("Polling...")
    $.ajax({
        type : 'GET',
        success : function(res){
            console.log("SUCCESS");
            console.log(res)
            $('#gameList').html(res);
            $(document).ready(function($) {
                $(".table-row").click(function() {
                    console.log("clicked")
                    // window.document.location = $(this).data("href");
                    game_id = $(this).data("href")
                    joinGame(game_id)
                });
            });
        },
        error: function(res){
            console.log("Error connecting to server") 
            console.log(res)
        }
    });

    

    setTimeout(function(){
        findGame();
    }, 5000);
}



findGame()

