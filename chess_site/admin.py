from django.contrib import admin
import chess_site.models as chess_models


admin.site.register(chess_models.TimeControl)

admin.site.register(chess_models.Game)

admin.site.register(chess_models.ErrorMessage)
# admin.site.register(chess_models.GameREc)
