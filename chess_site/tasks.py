
from celery.utils.log import get_task_logger
from chess_site.models import Game
from chess_site.chessclock import ChessClock
from chess_site.chessgamesmanager import ChessGamesManager
from celery import shared_task


@shared_task
def print_test(game_id):
    print("gameid: " + str(game_id))
    ChessClock.calculate_and_update_clock_from_gameid(game_id)

    
    
@shared_task
def update_clocks(game_id):
    print("gameid: " + str(game_id))
    game_id = game_id.replace(ChessGamesManager.TASK_PREFIX, '')
    ChessClock.calculate_and_update_clock_from_gameid(game_id)