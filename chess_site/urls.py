from django.urls import path, include

from chess_site.urls_generator import UrlsGenerator
from . import views
from . import api



urlpatterns = UrlsGenerator.construct_urlpatterns_r(views)
