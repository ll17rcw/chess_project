import chess
import chess.pgn
import re
import io
import pgn
import datetime
from .models import Game
# from enum import StrEnum

# class ChessOutcome(StrEnum):
#     chess.Termination.CHECKMATE = "CHECKMATE"
#     chess.Termination.STALEMATE = "STALEMATE"
#     chess.Termination.INSUFFICIENT_MATERIAL = "INSUFFICIENT_MATERIAL"
#     chess.Termination.SEVENTYFIVE_MOVES = "SEVENTYFIVE_MOVES"
    
class ChessValidatorException(Exception):
    pass
class InvalidBoardError(ChessValidatorException):
    pass

class InvalidFenError(ChessValidatorException):
    pass

class InvalidPgnError(ChessValidatorException):
    pass

class InvalidMoveError(ChessValidatorException):
    pass


class ChessValidator():
    

    
    DEFAULT_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

    # def __init__(self, fen=DEFAULT_FEN, _pgn=None):
    #     self.board = chess.Board(fen) 
    #     self.pgn_game = pgn.PGNGame()
        
        
    #     print("pgn_game : {} ".format(self.pgn_game.moves))
    #     self.pgn = chess.pgn.Game() #old method
    #     if pgn: 
    #         self.pgn = chess.pgn.read_game(io.StringIO(_pgn))
    #     self.node = self.pgn
    
    def __init__(self, game):
        self.board = chess.Board(game.current_fen)
        self.set_pgn(game)

    def get_pgn_headers_as_dict(self):
        response = {
            "site" : self.pgn_game.site,
            "date" : self.pgn_game.date,
            "white" : self.pgn_game.white,
            "black" : self.pgn_game.black,
            "fen" : self.pgn_game.fen
        }
        return response
    
    def get_pgn_headers_as_list(self):
        """
        Uses get_pgn_headers_as_dict to return a list version of pgn headers.
        This list representation allows the frontend to register it.
        """        
        response = []
        for key, value in self.get_pgn_headers_as_dict().items():
            response.append(key)
            response.append(value)
        
        return response
            
    
    def get_pgn_moves(self):
        return self.pgn_game.moves
    
    def get_pgn(self):
        # print("get_pgn: {}".format(self.pgn_game))
        # print("get pgn str: {}".format(self.pgn_game.dumps()))
        return self.pgn_game
    
    def get_pgn_moves(self):
        return self.pgn_game.moves
    
    def get_fen(self):
        return self.board.fen()
    
    def seperate_moves(self, moves):
        moves = moves.lstrip()
        moves = moves.rstrip()
        white_move = None
        black_move = None
        s_moves = moves.split(" ")
        # print(moves)
        # print("s moves: {}".format(s_moves))
        if (len(s_moves) > 2):
            raise AttributeError("More than 2 moves occurred")
        elif (len(s_moves) == 2):
            white_move = s_moves[0]
            black_move = s_moves[1]
        elif (len(s_moves) == 1):
            white_move = s_moves[0]
            
        return white_move, black_move
            
            
        
        
    
    # def get_pgn_moves_dict(self):
    #     mainline = self.pgn.mainline()
    #     mainline_split = re.split('[0-9]+\.', str(mainline))
    #     # mainline_list = re.search('[0-9]+\.', str(mainline))
    #     # print("ml list: {}".format(str(mainline)[:mainline_list.end(0)]))#
    #     result = {}
    #     count = 1
    #     # print("mainline: {}".format(mainline))
    #     # print("mainline split: {}".format(mainline_split))
    #     for move in mainline_split[1:]:
    #         white_move, black_move = self.seperate_moves(move)
    #         result[count] = {"move" : count,
    #                          "white" : white_move,
    #                          "black" : black_move}
    #         count += 1
    #     print(result)
        # return result
    
    # def set_board(self, fen, pgn):
    #     try:
    #         test_board = chess.Board(fen)
    #         if (not test_board.is_valid()):
    #             return False
    #         self.board.set_fen(fen)
    #         if pgn: 
    #             print("setting pgn: {}".format(pgn))
    #             # self.pgn = chess.pgn.Game()
    #             # self.pgn = chess.pgn.read_game(io.StringIO(pgn)) # TODO: validate this
    #             # self.node = self.pgn 
    #             print("pgn moves dict : {}".format(self.get_pgn_moves_dict()))  
    #         return True
    #     except ValueError:
    #         return False
    
    def set_pgn(self, game):
        if (type(game) is str):
            # print("game : {}".format(game))
            self.pgn_game = pgn.loads(game)[0]
            print(type(self.pgn_game))
            return True
        elif (type(game) is Game):
            if (game.pgn):
                # print("pgn found in set_pgn {}".format(game.pgn))
                # print("game pgn : {}".format(game.pgn))
                self.pgn_game = pgn.loads(game.pgn)[0]
                # print(self.pgn_game)
                return True
            else:
                # print("not pgn")
                self.pgn_game = pgn.PGNGame()
                self.pgn_game.white = game.player_w.username
                self.pgn_game.black = game.player_b.username
                self.pgn_game.site = "UoLChessApp"
                self.pgn_game.fen = game.current_fen
                # self.pgn_game.date = game.datetime_started
                return True
            # print("===== SET PGN =====")
            # print(self.pgn_game)
        else:
            return False
            
    def set_board(self, game):
        try:
            test_board = chess.Board(game.current_fen)            
            if (not test_board.is_valid()):
                # print("test board is not valid for fen:\n{}".format(game.current_fen))
                return False
            self.board.set_fen(game.current_fen)
            self.set_pgn(game)
            return True
        except ValueError as e:
            print(e)
            return False
            

    
    def make_move(self, source, target, pgn_):
        """
        Makes a move on the server-stored board and returns if the move has been made.
        """
        # print("pgn recieved in make move: {}".format(pgn_))
        move = "".join([source, target])
        # print("source: {} \ntarget: {}".format(source, target))
        uci_move = chess.Move.from_uci(move)
        print(uci_move)
        print(self.board.legal_moves)
        print(self.board.fen())
        if (uci_move in self.board.legal_moves):
            
            # print("THIS PGN : {}".format(pgn_))
            if not self.set_pgn(pgn_):
                print("not set pgn")
                return False
            self.board.push(uci_move)
            # self.node = self.node.add_variation(uci_move)
           
            # self.pgn_game.moves.append(str(uci_move.to_square))
            # self.pgn_game = pgn.loads(pgn_)
            # print("moves: {}".format(self.pgn_game.moves))
            # print(self.pgn_game)
            return True
        print("not in legal moves")
        return False
            
    def check_for_game_over(self):
        """
        Checks if the game is currently over and if there is a winner (and if so, then who)
        """
        if (not self.board.outcome()):
            return None
        winner = None
        
        # Find winner (none if draw)
        if (self.board.outcome().winner == True):
            winner = "w"
        elif (self.board.outcome().winner == False):
            winner = "b"
            
        # Find outcome
                
        return_data = {"outcome" : self.board.outcome().termination.name,
                       "winner" : winner}
        
        
        # print(str(return_data))
        return return_data
    
    # @staticmethod
    # def is_valid_move(old_fen, source, target):
    #     move = "".join([source, target])
        
    #     uci_move = chess.Move.from_uci(move)
    #     board = chess.Board(old_fen)
    #     if not board.is_valid():
    #         raise ValueError("Board FEN not valid")
    #     if (uci_move in board.legal_moves):
    #         return True
    #     return False
        
    def is_valid_move(fen=None, pgn_str=None, move=None):
        try:
            uci_move = chess.Move.from_uci(move)
        except ValueError:
            raise InvalidMoveError("Move does not conform to UCI standard.")
        try:
            board = chess.Board(fen)
        except ValueError:
            raise InvalidBoardError("Board supplied is not valid")
        if not board.is_valid():
            raise InvalidBoardError("Board supplied is not valid")
        if (uci_move in board.legal_moves):
            return True
        return False
    
    # def is_valid_move(fen, move):
        
        
    
    # def is_valid_uci_move(fen, move):
        
    
    def make_uci_move(game, move):
        fen = game.current_fen
        try:
            uci_move = chess.Move.from_uci(move)
        except ValueError:
            return False
        try:
            board = chess.Board(fen)
        except ValueError:
            raise InvalidBoardError("Board is not valid")
        if not board.is_valid():
            raise InvalidBoardError("Board supplied is not valid")
        if (uci_move in board.legal_moves):
            board.push_uci(move)
            game.current_fen = board.fen
            game.save()
            return game
        print("uci move not in legal moves")
        return None

    def is_valid_pgn(pgn_str : str):
        pgn_text = io.StringIO(pgn_str)
        game = chess.pgn.read_game(pgn_text)
        return True
    
    def is_valid_fen(fen : str):
        board = chess.Board(fen)
        return board.is_valid()
    
    def get_legal_moves(fen : str):
        try:
            board = chess.Board(fen)
            if not board.is_valid():
                raise InvalidBoardError("Fen supplied is not valid")
            return board.legal_moves
        except ValueError:
            raise InvalidBoardError("Fen supplied is not valid")
        
        
        

    
        
        
    