// function Clock(duration, granularity) {
//     this.duration = duration; // in deciseconds
//     this.granularity = 100;
//     this.tickFtns = [];
//     this.running = false;
//     this.last_time = Date.now()
//     this.that = null;
//   }
  
//   function runTimer(){
//     console.log("runtimer ran")
//         var millisecond_diff, obj;
//     if (!this.that.running){
//         console.log("not running")
//         return
//     }
//     // console.log(Date.now())
//     // console.log(start)
//     // console.log((Date.now() - start) / 100)
//     millisecond_diff = this.that.duration - (((Date.now() - this.last_time) / 100) | 0);
//     console.log("diff: " + millisecond_diff)
//     if (millisecond_diff > 0) {
//         console.log("here")
//         setTimeout(runTimer, this.that.granularity);
//     } else {
//         diff = 0;
//         this.that.running = false;
//     }
//     obj = Clock.parse(millisecond_diff);
//     console.log(this.that.tickFtns)
//     this.that.tickFtns.forEach(function(ftn) {
//         ftn.call(this, obj.minutes, obj.seconds, obj.deciseconds);
//     }, this.that);
//   };

//   Clock.prototype.start = function() {
//     if (this.running) {
//       return;
//     }
//     this.running = true;
//     var start = Date.now(),
//         that = this,
//         diff, obj;
//     this.last_time = Date.now()
//     this.that = this
//     this.runTimer()
//     // (function timer() {
//     //     if (!that.running){
//     //         return
//     //     }
//     //     // console.log(Date.now())
//     //     // console.log(start)
//     //     // console.log((Date.now() - start) / 100)
//     //     last_time = Date.now()
//     //   millisecond_diff = that.duration - (((last_time - start) / 100) | 0);
//     //     console.log("diff: " + millisecond_diff)
//     //   if (millisecond_diff > 0) {
//     //     setTimeout(timer, that.granularity);
//     //   } else {
//     //     diff = 0;
//     //     that.running = false;
//     //   }
//     //   obj = Clock.parse(millisecond_diff);
//     //   console.log(that.tickFtns)
//     //   that.tickFtns.forEach(function(ftn) {
//     //     ftn.call(this, obj.minutes, obj.seconds, obj.deciseconds);
//     //   }, that);
//     // }());
    
//   };

//   Clock.prototype.pause = function(){
//       if (!this.running){
//           return
//       }
//       this.running = false
//       return;
//   }

//   Clock.prototype.restart = function(){
//       if (this.running){
//           return
//       }
//   }
  
//   Clock.prototype.onTick = function(ftn) {
//       console.log("ontick")
//     if (typeof ftn === 'function') {
//       this.tickFtns.push(ftn);
//     }
//     return this;
//   };
  
//   Clock.prototype.expired = function() {
//     return !this.running;
//   };
  
//   Clock.parse = function(deciseconds) {
//     return {
//       'minutes': (deciseconds / 600) | 0,
//       'seconds': (deciseconds / 10) | 0,
//       'deciseconds': (deciseconds % 10) |0
//     };


//   };

class Clock {
    constructor(time, increment){
        this.duration = time;
        this.granularity = 100;
        this.increment = increment;
        this.running = false;
        this.tickFtns = [];
        this.test = null;
        this.time_paused = null;
        this.start_time = null;
        this.millisecond_diff = null;
    }


    start(time) {
        if (time == null){
            time = this.duration
        }
        // console.log("time: " + time)
        // console.log("ms diff: " + this.millisecond_diff)
        // if (!this.start_time){
        //     this.start_time = Date.now()
        // }
        
        // if (this.time_paused){
        //     var start_time = this.time_paused - this.start_time
        //     console.log("start time " + this.start_time)
        //     console.log("time paused " + this.time_paused)
        //     console.log(start_time)
        // }
        // else{
        //     var start_time = this.start_time
        // }

        if (this.running) {
          return;
        }
        this.running = true;
        var start = Date.now(),
            last_time = Date.now(),
            that = this,
            obj;
        
      
        (function timer() {
            if (!that.running){
                return
            }
            // console.log(Date.now())
            // console.log(start)
            // console.log((Date.now() - start) / 100)
        
          that.millisecond_diff = time - (((Date.now() - start)) | 0);
          // console.log("ms diff: " + that.millisecond_diff);
          // console.log("diff: " + that.millisecond_diff)
          if (that.millisecond_diff > 0) {
            setTimeout(timer, that.granularity);
          } else {
            that.millisecond_diff = 0;
            that.running = false;
          }
          obj = Clock.parse(that.millisecond_diff);
        //   console.log(that.tickFtns)
          that.tickFtns.forEach(function(ftn) {
            ftn.call(this, obj.minutes, obj.seconds, obj.deciseconds);
          }, that);
        }());
      };

    pause(){
        this.running = false;
        this.time_paused = Date.now()
        return
    }


    onTick(ftn){
        if (typeof ftn === 'function'){
            this.tickFtns.push(ftn);
        }
        return this;
    };

    expired(){
        return !this.running;
    };

    getTime(){
        return this.millisecond_diff;
    }

    static parse(milliseconds) {
      var deciseconds = (milliseconds / 100) % 10
      var seconds = (milliseconds /1000) % 60
      var minutes = (milliseconds / (1000*60)) % 60
        // var minutes = (deciseconds / 1000) / 60;

        // //console.log("mins: " + minutes)
        // var seconds = (deciseconds / 10) % 60;
        // var deciseconds = 
        
        return {
            'minutes': minutes | 0,
            'seconds': seconds | 0,
            'deciseconds': deciseconds |0
          };
    };

    static format(minutes, seconds, deciseconds) {
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        deciseconds = deciseconds < 10 ? + deciseconds : deciseconds;
        return minutes + ':' + seconds + '.' + deciseconds;
    };
}
  