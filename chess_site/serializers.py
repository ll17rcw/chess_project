from chess_site.chessclock import ChessClock
from chess_site.models import User, Game
from rest_framework import serializers
from typing import Union, List
from django.db.models.query import QuerySet

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email',]


# class GroupSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Group
#         fields = ['url', 'name']

class ChessGamesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ['game_id', 'game_status', 'player_w', 'player_b']
        
class GameSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer to represent a single (active) chess game of a user
    """
    class Meta:
        model = Game
        fields = ['game_id', 'current_fen', 'game_status',
                  'time_started', 'turn', 'white_connected', 'black_connected']
        
# class ChessGameSerializer():
#     def __init__(self, game : )
        

class ChessClockSerializer(serializers.Serializer):
    """
    Serializer to represent a game chess clock
    """
    white_clock = serializers.IntegerField()
    black_clock = serializers.IntegerField()
    time_calculated_with = serializers.IntegerField()
    
class ChessGameSerializer():
    def __init__(self, game :  Union[Game, QuerySet, List[Game]], many : bool = False, time_with: bool = True):
        if many and (isinstance(game, QuerySet) or (isinstance(game, list))):
            self.data = []
            for g in game:
                
                game_data = GameSerializer(g).data
                
                white_clock, black_clock, time_calculated_with = ChessClock.calculate_clocks(g)
                if time_with:
                    clock_data = {
                        'white_clock' : white_clock,
                        'black_clock' : black_clock,
                        # 'time_calculated_with' : time_calculated_with
                        }
                else:
                    clock_data = {
                    'white_clock' : white_clock,
                    'black_clock' : black_clock,
                    }
                self.data.append({
                    'game' : game_data,
                    'clock' : clock_data
                })
         
        else:
            if not isinstance(game, Game):
                raise ValueError("Game should be type Game or QuerySet (of Game)")
            white_clock, black_clock, time_calculated_with = ChessClock.calculate_clocks(game)
            if time_with:
                self.data = {
                    'game_id' : game.game_id,
                    'white_clock' : white_clock,
                    'black_clock' : black_clock,
                    # 'time_calculated_with' : time_calculated_with
                }
            else:
                self.data = {
                    'game_id' : game.game_id,
                    'white_clock' : white_clock,
                    'black_clock' : black_clock,
                }
            
    
class ChessClockModelSerializer(serializers.ModelSerializer):
    pass
        


    
        

