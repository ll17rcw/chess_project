import os
import json

class Data():
    def __init__(self):
        self.data = {}
        self.data.update(self.construct_data_from_directory(directory="endpoints"))
        
    
    def get_all_data_files(self):
        rootdir = 'C:/Users/sid/Desktop/test'
        print("cwk: {}".format(os.getcwd()))
        for subdir, dirs, files in os.walk("{}".format(os.path.dirname(os.path.realpath(__file__)))):
            for file in files:
                if file.endswith(".json"):
                    print(os.path.join(subdir, file))
                    json_file_path = os.path.join(subdir, file)
                    with open(json_file_path) as json_file:
                        json_data = json.load(json_file)
                        print(file)
                        self.data[os.path.splitext(file)[0]] = {}
                        self.data[os.path.splitext(file)[0]]["file_path"] = json_file_path
                        self.data[os.path.splitext(file)[0]]["data"] = json_data
                        
                        # print("FILE RSTRIP: {}".format(file.rstrip(".json")))
                        # self.data[file.rstrip(".json")] = json_data
                    
    def get_files(self, directory, extension = None):
        files = []
        for filename in os.listdir(directory):
            f = os.path.join(directory, filename)
            if extension:
                if os.path.isfile(f) and filename.endswith(extension):
                    files.append(filename)
            else:
                if os.path.isfile(f):
                    files.append(filename)
            
        return files
    
    def construct_data_from_directory(self, directory, extension='.json'):
        data = {}
        abs_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), directory)
        dir_name = directory
        data[dir_name] = {}
        for filename in os.listdir(abs_directory):
            f = os.path.join(abs_directory, filename)
            if extension:
                if not filename.endswith(extension):
                    continue
            if os.path.isfile(f):
                with open(f) as json_file:
                    print(f)
                    json_data = json.load(json_file)
                    print(os.path.splitext(filename)[0])
                    data[dir_name][os.path.splitext(filename)[0]] = json_data
                    
        return data
    
    def get_endpoints(self):
        return self.data["endpoints"]
                    
    
    # def __repr__(self):
    #     return self.data

SITE_DATA = Data().data