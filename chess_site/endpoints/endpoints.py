import sys
from types import ModuleType

from django.urls import path

from chess_site.data.data import SITE_DATA

class Endpoint():
    def __init__(self, endpoint_name : str, endpoint_data : dict, module : ModuleType) -> None:
        try:
            self.endpoint_data = endpoint_data
            print("ENDPOINT NAME: {}".format(endpoint_name))
            self.handler_name = endpoint_data["handler"]
            func = getattr(module, self.handler_name)
            self.handler = func
            self.description = endpoint_data["description"]
            self.parameters = endpoint_data["parameters"]
            self.accepted_methods = endpoint_data["accepted_methods"]
        except Exception as e:
            print("ERROR: Could not load endpoints correctly. Check the JSON files")
            print(e)
            sys.exit()
        
    def get_endpoint(self) -> str:
        return self.endpoint_name

    def get_handler(self):
        return self.handler
    
    def get_handler_name(self):
        return self.handler_name

    def get_description(self):
        return self.description
    
    def get_parameters(self):
        return self.parameters
    
    def get_accepted_methods(self):
        return self.accepted_methods


class Endpoints():
    
    def __init__(self, module: ModuleType) -> None:
        if prefix is None:
            prefix = self.get_name_from_module(module)
        print("IN CONSTRUCT URLPATTERNS ENDPIINTS\n\n\n")
        endpoints_key = "{}_endpoints".format(prefix)
        endpoints_data = SITE_DATA["endpoints"][endpoints_key]
        print(endpoints_data)
        self.endpoints = []
        
        for endpoint, endpoint_data in endpoints_data.items():
            print("\n\n{}\n\n".format(endpoint))
            endpoint_obj = Endpoint(endpoint_name=str(endpoint),
                                    endpoint_data=endpoint_data,
                                    module=module)
            self.endpoints.append(endpoint_obj)
            
    def construct_urlpatterns(self):
        urlpatterns = []
        for endpoint in self.endpoints:
            endpoint_name = endpoint.get_name()
            endpoint_handler = endpoint.get_handler
            urlpatterns.append(path(endpoint_name, endpoint_handler))
        return urlpatterns
        
    def get_name_from_module(self, module):
        return module.__name__.split(".")[-1]