
import json
import os
import shutil
from pathlib import Path

from django.shortcuts import render
from chess_site.data.data import SITE_DATA, Data
from django.template import Template, Context, RequestContext
from django.template.loader import render_to_string
from django.conf import settings



class DocumentationGenerator():


    
    def __init__(self):
        # file = open(filepaths_path)     
        # self.filepaths = json.read(file)
        # self.data = Data().endpoints
        self.data = SITE_DATA
        self.current_file_path = Path(__file__).parent.resolve()

        pass
        
    def generate_file(self, name, data):
        print("Generating file {}".format(name))
        output_file = Path.joinpath(self.output_path, Path(name))
        with output_file.open('w', encoding='utf-8') as f:
            f.write(data)
        
    def generate_output_directory(self):
        output_path = Path.joinpath(self.current_file_path, "output")
        if os.path.exists(output_path):
            shutil.rmtree(output_path)
        os.makedirs(output_path)
        
        return output_path 
    
    def generate_endpoints(self):
        endpoint_template = Template(Path.joinpath(self.current_file_path, "templates/endpoints.html"))
        print("endpoints:")
        data = {}
        endpoints = self.data["endpoints"]
        for endpoint_parent in endpoints:
            data[endpoint_parent] = {}
            data[endpoint_parent]["PREFIX"] = str(endpoint_parent).split("_")[0] if str(endpoint_parent).split("_")[0] != "views" else ""
            data[endpoint_parent]["endpoints"] = endpoints[endpoint_parent]
        
        context = {"endpoints" : data}
        print(context)
        # endpoint_template.render(endpoints)
        html = render_to_string("documentation/endpoints.html", context=context)
        self.generate_file("endpoints.html", html)
        
        
    
    
    def generate_documentation(self):
        self.output_path = self.generate_output_directory()
        # self.copy_templates()
        self.generate_endpoints()


