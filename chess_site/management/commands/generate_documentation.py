
from django.core.management.base import BaseCommand
from documentation.documentation_generator import DocumentationGenerator

class Command(BaseCommand):
    def handle(self, **options):
        doc_gen = DocumentationGenerator()
        doc_gen.generate_documentation()
        # now do the things that you want with your models here
    