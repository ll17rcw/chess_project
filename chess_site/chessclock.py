from re import S
import time
from chess_site.models import Game, GameStatus
from django.db import transaction

import redis


class ChessClockException(Exception):
    pass

class GameNotStarted(ChessClockException):
    pass

class InvalidClockException(ChessClockException):
    pass


class ChessClock():
    """
    
    """
    def __init__(self, game):
        self.player_w = game.player_w
        self.player_b = game.player_b
        self.time = game.game_time.time * 1000
        self.increment = game.game_time.increment * 1000
        self.time_white = game.white_clock
        self.time_black = game.black_clock
        self.time_started = game.time_started
        # self.game_start_time = game.game_start_time
        # self.white_last_time = game.white_last_time
        # self.black_last_time = game.black_last_time
        # self.time_white_used = game.time_white_used
        # self.time_black_used = game.time_black_used
        # self.white_move_start_time = game.white_move_start_time
        # self.black_move_start_time = game.black_move_start_time     
        # self.white_clock = game.white_clock
        # self.black_clock = game.black_clock   
        
        
        
    # def make_move(self, player):
    #     if player == self.player_w:
    #         time_white_used_for_move = round(time.time() / 100) -self.white_last_time
    #         self.time_white_used += time_white_used_for_move
    #         self.black_last_time = round(time.time() / 100)
    #         self.white_clock = self.calc_white_clock()
    #         return True
            
    #     if player == self.player_b:
    #         time_black_used_for_move = round(time.time() / 100) -self.black_last_time
    #         self.time_black_used += time_black_used_for_move
    #         self.white_last_time = round(time.time() / 100)
    #         self.black_clock = self.calc_black_clock()
    #         return True
    
    def make_move(self, player, time_on_client_clock):
        if player == self.player_w:
            self.time_white = self.time_white - (time.time() - self.time_of_last_move_white)
            self.time_of_last_move_white = time.time()
            return True
        elif player == self.player_b:
            self.time_black = self.time_black - (time.time() - self.time_of_last_move_black)
            self.time_of_last_move_black = time.time()
            return True
            
            
            
    def get_white_clock(self):
        return self.time_white

    def get_black_clock(self):
        return self.time_black
    
    def get_time_of_last_move_white(self):
        return self.time_of_last_move_white
    
    def get_time_of_last_move_black(self):
        return self.time_of_last_move_black
    
    def get_time_started(self):
        return self.time_started
    
    # def get_white_clock(self):
    #     return self.white_clock

    # def get_black_clock(self):
    #     return self.black_clock
    
    # def calc_white_clock(self):
    #     return (self.time / 600) - (self.time_white_used)

    # def calc_black_clock(self):
    #     return (self.time / 600) - (self.time_black_used)
    
    
    # def get_white_last_time(self):
    #     return self.white_last_time
    
    # def get_black_last_time(self):
    #     return self.black_last_time
    
    # def get_white_relative_time(self):
    #     if self.white_last_time == -1:
    #         return round(time.time() *10) - self.game_start_time
    #     elif (self.move == "b"):
    #         return self.get_white_last_time()
    #     else:
    #         diff = round(time.time()*10) - self.white_last_time
    #         print("diff: {}".format(diff))
    #         return diff
    #     print("white relative time: {}".format((self.time * 10) - ((self.white_last_time - self.game_start_time) / 100)))
    #     return (self.time * 10) - ((self.white_last_time - self.game_start_time) / 100)
    
    # def get_black_relative_time(self):
    #     if self.black_last_time == -1:
    #         return round(time.time() *10) - self.game_start_time
    #     else:
    #         return self.get_black_last_time()
        # else: # get the difference between blacks last move and now
        #     diff = round(time.time() *10) - self.black_last_time
        #     print("diff: {}".format(diff))
        #     return diff
        # print("black relative time: {}".format((self.time * 10) - ((self.black_last_time - self.game_start_time) / 100)))

        # return (self.time * 10) - ((self.black_last_time - self.game_start_time) / 100)
    
    # def get_game_start_time(self):
    #     return self.game_start_time
    
    # def get_initial_time(self):
    #     return self.time * 600

    # def get_initial_increment(self):
    #     return self.increment
    
    # def make_move_white(self):
    #     self.white_last_time = round(time.time() / 100)
    #     return self.white_last_time
        
    # def make_move_black(self):
    #     self.black_last_time = round(time.time() / 100)    
    #     return self.black_last_time
        
    
    

    # def set_black_move(self):
    #     self.move = "b"
    
    # def set_white_move(self):
    #     self.move = "w"
        
    def get_turn(self):
        return self.move
    
    def start_clock(self):
        self.time_started = time.time()
    # def start_clock(self):
    #     print("game start time set to: {}".format(round(time.time() * 10)))
    #     self.game_start_time = round(time.time() / 100) #TODO: change to milliseconds

    # def set_white_move_times(self, white_move_times):
    #     self.white_move_times = white_move_times
    
    # def set_black_move_times(self, black_move_times):
    #     self.black_move_times = black_move_times
    
    
    # def get_time_from_start(self):
    #     return time.monotonic() - self.game_start_time
    
    
    # def update_clock_db(self):
    #     if self.game_start_time is None:
    #         return False
    
    @staticmethod
    def set_game_over(game, winner):
        if winner is None:
            game.winner = 'd'
        elif winner == game.player_w:
            game.winner = 'w'
        elif winner == game.player_b:
            game.winner = 'b'
        else:
            raise ValueError("Winner is not in that game.")
        
        game.game_status = GameStatus.COMPLETED
        game.save()
        return game
            
    

        
    @staticmethod
    def calculate_clocks(game, time_received = time.time_ns() // 1_000_000):
        """
        Calculates the clocks for both users but does *not* update them in the database.
        Calculates the clocks by 
        Calculating the difference between the users stored clock and the 
        

        Args:
            game (_type_): _description_

        Raises:
            ValueError: _description_

        Returns:
            _type_: _description_
        """
        # if game.time_started == -1:
        #     game_started = False
        # else:
        #     game_started = True
        game_status = game.game_status
        time_started = game.time_started
        white_clock = game.white_clock
        black_clock = game.black_clock
        if game_status == GameStatus.FORMED or game_status == GameStatus.PENDING:
            if time_started > 0:
                raise ChessClockException("Game has not started but has start time.")
            return white_clock, black_clock, time_received
        if game.turn == 'w':
            time_diff = time_now = game.white_last_time # TODO: what is this?
            white_clock = game.white_clock - time_diff if (game.white_clock - time_diff > 0) and (game.white_clock != 0) else 0
            black_clock = game.black_clock
        elif game.turn == 'b':
            time_diff = time_now = game.black_last_time # TODO: what is this?
            black_clock = game.black_clock - time_diff if (game.black_clock - time_diff > 0) and (game.black_clock != 0) else 0
            white_clock = game.white_clock
        
        return white_clock, black_clock, time_received
    
    @staticmethod
    def calculate_and_update_clock(game):
        """
        Calculates both players clock and updates memory copy of game.
        .save() method must be called afterwards.

        Args:
            game (chess_site.models.Game): The game to update clocks for.

        Returns:
            game (chess_site.models.Game) : The game with updated clocks.
        """
        if game.time_started == -1:
            # raise ValueError("Game has not started")
            print("game has not started")
            return game
        time_now = time.time_ns() // 1_000_000
        
        
        white_clock, black_clock, time_used = ChessClock.calculate_clocks(game, time_received=time_now)
        
        game.white_clock = white_clock
        game.black_clock = black_clock
        
        # if (game.white_clock == 0):
        #     print("BLACK WON")
        #     ChessClock.set_game_over(game, game.player_b)
        #     ChessClock.alert_game_over(game, 'b')
        # elif (game.black_clock == 0):
        #     print("WHITE WON")
        #     ChessClock.set_game_over(game, game.player_w)
        #     ChessClock.alert_game_over(game, 'w')
        # else:
        #     print("NOONE HAS WON")
        #     print("white clock : {} - black clock: {}".format(game.white_clock, game.black_clock))
            
        return game
    
    @transaction.atomic 
    def check_and_alert_timeout(game):
        if (game.time_started == -1):
            raise ValueError("Game has not started")
        if (game.white_clock == 0):
            print("BLACK WON")
            ChessClock.set_game_over(game, game.player_b)
            ChessClock.alert_game_over(game, 'b')
        elif (game.black_clock == 0):
            print("WHITE WON")
            ChessClock.set_game_over(game, game.player_w)
            ChessClock.alert_game_over(game, 'w')
        else:
            print("NOONE HAS WON")
            print("white clock : {} - black clock: {}".format(game.white_clock, game.black_clock))  

        
    @transaction.atomic
    def calculate_and_update_clock_from_gameid(game_id):
        try:
            print("getting game with id {}".format(game_id))
            game = Game.objects.get(pk=game_id)
            
        except Game.DoesNotExist:
            print("Game does not exist")
        
        ChessClock.calculate_and_update_clock(game)