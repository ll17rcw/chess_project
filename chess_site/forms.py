from django import forms 
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import TimeControl

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()
    first_name = forms.CharField()
    surname = forms.CharField()
    
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'surname', 'password1', 'password2']


class GameCreationForm(forms.Form):
    game_times = TimeControl.objects.all()
    time_list = []
    for game_time in game_times:
        time_list.append((game_time.id,f"{game_time.time}+{game_time.increment}")) # TODO: Make it so this isnt ran every time ?
    print(time_list)
    time_control = forms.ChoiceField(choices=time_list)
    colour = forms.ChoiceField(choices=[('w','white'), ('b','black')])
    # game_id = forms.CharField()
    
    
def milliseconds_to_seconds(milliseconds):
    return milliseconds/1000

class GameFinderForm(forms.Form):
    game_times = TimeControl.objects.all()
    time_list = []
    for game_time in game_times:
        time_list.append((game_time.id,f"{game_time.time}+{game_time.increment}")) # TODO: Make it so this isnt ran every time ?
    print(time_list)
    time_control = forms.ChoiceField(choices=time_list)
        


        
        
    