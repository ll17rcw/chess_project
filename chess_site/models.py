from django.db import models, transaction
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.crypto import get_random_string
import uuid
import enum
from django_celery_beat.models import PeriodicTask, IntervalSchedule



schedule, created = IntervalSchedule.objects.get_or_create(
    every=1,
    period=IntervalSchedule.SECONDS,
)


class TimeControl(models.Model):
    time = models.IntegerField(default = 1) # time in seconds
    increment = models.IntegerField(default = 1) # increment in seconds
    

class GameStatus(models.IntegerChoices):
    FORMED = 0, 'Formed' # Formed but not initiated. Usually after 1 user has created game
    PENDING = 1, 'Pending' # Game is assigned to 2 users but not yet started. (not connected)
    ONGOING = 2, 'Ongoing' # Game is currently ongoing. (clock ticking)
    COMPLETED = 3, 'Completed' # Game is completed.

class MatchmakingStatus(models.IntegerChoices):
    AWAITING = 0, 'Awaiting'
    COMPLETED = 1, 'Completed'

class MatchmakingInstance(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    matchmaking_status = models.IntegerField(default=MatchmakingStatus.AWAITING, choices=GameStatus.choices)


class Game(models.Model):
    """
    Represents an active game of chess played by 2 players (player_w and player_b).

    """  
    
    # https://stackoverflow.com/questions/31499836/does-uuidfields-default-attribute-takes-care-of-the-uniqueness
    game_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False) 
    game_time = models.ForeignKey(TimeControl, on_delete=models.CASCADE, related_name="games")
    player_w = models.ForeignKey(User, on_delete=models.CASCADE, related_name="game_player_white", null=True)
    player_b = models.ForeignKey(User, on_delete=models.CASCADE, related_name="game_player_black", null=True)
    current_fen = models.CharField(default="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" ,max_length=2056)
    pgn = models.CharField(default= None, max_length=2056, blank = True, null = True)
    time_created = models.DateTimeField(default=timezone.now)
    game_started = models.BooleanField(default=False)
    time_started = models.IntegerField(default=-1)
    # game_status = models.IntegerChoices(default=GameStatus.PENDING.value)
    winner = models.CharField(default="w", max_length=1)
    game_status = models.IntegerField(default=GameStatus.FORMED, choices=GameStatus.choices)
    white_last_time = models.IntegerField(default=-1)
    black_last_time = models.IntegerField(default=-1)
    white_clock = models.IntegerField(default=-1)
    black_clock = models.IntegerField(default=-1)
    white_connected = models.BooleanField(default=False)
    black_connected = models.BooleanField(default=False)
    game_turn = models.CharField(default="w", max_length=1)
    # time_white = models.IntegerField(default=-1)
    # time_black = models.IntegerField(default=-1)
    # time_of_last_move_white = models.IntegerField(default=-1)
    # time_of_last_move_black = models.IntegerField(default=1)
    # game_start_time = models.IntegerField(default=-1)
    # white_last_time = models.IntegerField(default=-1)
    # black_last_time = models.IntegerField(default=-1)
    # time_white_used = models.IntegerField(default=0)
    # time_black_used = models.IntegerField(default=0)
    # white_move_start_time = models.IntegerField(default=-1)
    # black_move_start_time = models.IntegerField(default=-1)
    # white_clock = models.IntegerField(default=-1)
    # black_clock = models.IntegerField(default=-1)

    turn = models.CharField(default="w", max_length=1)

# class GameClock(models.Model):
#     game_id = models.OneToOneField(Game, primary_key=True, on_delete=models.CASCADE)
 
   

class GameRecord(models.Model):
    id = models.IntegerField(primary_key=True)
    player_w = models.ForeignKey(User, on_delete=models.CASCADE, related_name="gamerecord_player_white")
    player_b = models.ForeignKey(User, on_delete=models.CASCADE, related_name="gamerecord_player_black")
    time_control = models.ForeignKey(TimeControl, on_delete=models.CASCADE)


class ErrorMessage(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    details = models.TextField()
    status = models.IntegerField()
    
    
