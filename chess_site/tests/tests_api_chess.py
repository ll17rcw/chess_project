from django.test import TestCase, Client
from chess_site.models import GameStatus, TimeControl, User, Game
from rest_framework import status
import random
from chess_site.responses import CustomResponse, ErrorResponse, SuccessResponse
from chess_site.serializers import ChessGameSerializer


class CreateNewGameTest(TestCase):
    def setUp(self):
        timecontrol_1 = TimeControl.objects.create(
            time=1,
            increment=1
        )
        
        username = "user1"
        password = "user1pass"
        email = "user1@email.com"
        
        user1 = User.objects.create(
            username=username,
            email=email
        )
        
        user1.set_password(raw_password=password)
        user1.save()
        
        self.client = Client()
        self.client.login(username=username, password=password)
        
    def test_create_game_white(self):
        time = 1
        increment = 1
        colour = "w"
        
        response = self.client.post("/api/newgame/", {
            "time" : time,
            "increment" : increment,
            "colour" : colour
        })
        
        expected_status_code = status.HTTP_201_CREATED
        expected_response_id = CustomResponse(SuccessResponse.GAME_CREATED).id
        
        # self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
            
    def test_create_game_black(self):
        time = 1
        increment = 1
        colour = "b"
        
        response = self.client.post("/api/newgame/", {
            "time" : time,
            "increment" : increment,
            "colour" : colour
        })
        
        expected_status_code = status.HTTP_201_CREATED
        expected_response_id = CustomResponse(SuccessResponse.GAME_CREATED).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
class CreateNewExistingGameTest(TestCase):
    def setUp(self):
        timecontrol_1 = TimeControl.objects.create(
            time=1,
            increment=1
        )
        
        username_1 = "user1"
        password_1 = "user1pass"
        email_1 = "user1@email.com"
        
        user1 = User.objects.create(
            username=username_1,
            email=email_1
        )
        user1.set_password(raw_password=password_1)
        user1.save()

        
        username_2 = "user2"
        password_2 = "user2pass"
        email_2 = "user2@email.com"
        
        user2 = User.objects.create(
            username=username_2,
            email=email_2
        )
        user2.set_password(raw_password=password_2)
        user1.save()
        
        game = Game.objects.create(
            game_time = timecontrol_1,
            player_w = user1,
            player_b = user2,
            game_status = GameStatus.ONGOING,
            white_connected = True,
            black_connected = True
        )
        
        self.client = Client()
        self.client.login(username=username_1, password=password_1)
        # client.force_login(user1)

    def test_create_new_game_game_exists_white(self):
        time = 1
        increment = 1
        colour = "w"
        
        response = self.client.post("/api/newgame/", {
            "time" : time,
            "increment" : increment,
            "colour" : colour
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.GAME_ALREADY_EXISTS).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
    def test_create_new_game_game_exists_black(self):
        time = 1
        increment = 1
        colour = "b"
        
        response = self.client.post("/api/newgame/", {
            "time" : time,
            "increment" : increment,
            "colour" : colour
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.GAME_ALREADY_EXISTS).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
    def test_create_new_game_game_exists_timecontrol(self):
        time = 2
        increment = 2
        colour = "w"
        
        response = self.client.post("/api/newgame/", {
            "time" : time,
            "increment" : increment,
            "colour" : colour
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.GAME_ALREADY_EXISTS).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
class InvalidCreateNewGameTest(TestCase):
    def setUp(self):
        timecontrol_1 = TimeControl.objects.create(
            time=1,
            increment=1
        )
        
        username_1 = "user1"
        password_1 = "user1pass"
        email_1 = "user1@email.com"
        
        user1 = User.objects.create(
            username=username_1,
            email=email_1
        )
        
        user1.set_password(raw_password=password_1)
        user1.save()
        
        self.client = Client()
        self.client.login(username=username_1, password=password_1)
        
    def test_create_invalid_new_game_timecontrol(self):
        time = 2
        increment = 1
        colour = "w"
        
        response = self.client.post("/api/newgame/", {
            "time" : time,
            "increment" : increment,
            "colour" : colour
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.INVALID_GAME_INFO).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
    def test_create_invalid_new_game_colour(self):
        time = 1
        increment = 1
        colour = "p"
        
        response = self.client.post("/api/newgame/", {
            "time" : time,
            "increment" : increment,
            "colour" : colour
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.INVALID_GAME_INFO).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
class RetrieveAllGames(TestCase):
    
    def create_new_game(self, player_w, player_b, time_control):
        return Game.objects.create(
            player_w=player_w,
            player_b=player_b,
            game_time=time_control
        )
        
        
    def setUp(self):
        self.maxDiff = None
        timecontrol_1 = TimeControl.objects.create(
            time=1,
            increment=1
        )
        timecontrol_2 = TimeControl.objects.create(
            time=2,
            increment=2
        )
        
        client_username = "client_user"
        client_password = "client_user_password"
        client_email = "clientuser@email.com"
        
        client_user = User.objects.create(
            email=client_email
        )
        client_user.set_password(raw_password=client_password)
        client_user.save()
        
        self.client = Client()
        self.client.login(username=client_username, password=client_password)
        
        # random_n = random.randrange(100, 200, 2)
        random_n = 4
        
        self.num_users = random_n * 6
        self.total_num_games = random_n * 4
        self.num_double_games = random_n * 2
        self.num_single_white_games = random_n
        self.num_single_black_games = random_n
        
        self.users = []
        for i in range(0, self.num_users):
            username = "user_{}".format(i)
            password = "password_{}".format(i)
            email = "user{}@email.com".format(i)
            new_user = User.objects.create(
                username=username,
                email=email
            )
            new_user.set_password(raw_password=password)
            new_user.save()
            self.users.append(new_user)
        
        
        self.tc1_games = []
        self.tc1_games_cpy = []
        self.tc2_games = []
        self.tc2_games_cpy = []
        for i in range(self.total_num_games):
            if i < self.total_num_games / 2:
                self.tc1_games.append(self.create_new_game(None, None, timecontrol_1))
            else:
                self.tc2_games.append(self.create_new_game(None, None, timecontrol_2))
        self.all_games = self.tc1_games + self.tc2_games
        self.tc1_games_cpy = self.tc1_games
        self.tc2_games_cpy = self.tc2_games
        
        for i in range(self.num_double_games):
            player_w = self.users.pop()
            player_b = self.users.pop()
            if i % 2 == 0:
                game = self.tc1_games.pop()
            else:
                game = self.tc2_games.pop()
            
            game.player_w=player_w
            game.player_b=player_b
            game.game_status=GameStatus.ONGOING
            game.save()
            
        self.awaiting_black_games = []
        for i in range(self.num_single_white_games):
            player_w = self.users.pop()
            if i % 2 == 0:
                game = self.tc1_games.pop()
            else:
                game = self.tc2_games.pop()
            self.awaiting_black_games.append(game)
            game.player_w=player_w
            game.game_status=GameStatus.PENDING
            game.save()
        
        self.awaiting_white_games = []
        for i in range(self.num_single_white_games):
            player_b = self.users.pop()
            if i % 2 == 0:
                print("popping tc1")
                game = self.tc1_games.pop()
            else:
                print("popping tc2")
                game = self.tc2_games.pop()
            self.awaiting_white_games.append(game)
            game.player_b=player_b
            game.game_status=GameStatus.PENDING
            game.save()
            
    def test_get_all_games(self):
        
        response = self.client.get("/api/games/", {
            "time_with" : False
        })
        
        expected_status_code = status.HTTP_200_OK
        expected_response_id = CustomResponse(SuccessResponse.ALL_GAMES_RESPONSE).id
        expected_body = ChessGameSerializer(self.all_games, many=True, time_with=False).data
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        self.assertEqual(response.data["response_body"], expected_body)
        
        
    def test_get_all_games_awaiting_white(self):
        response = self.client.get("/api/games/",{
            "awaiting_player" : "white",
            "time_with" : False
        })        
        
        expected_status_code = status.HTTP_200_OK
        expected_response_id = CustomResponse(SuccessResponse.ALL_GAMES_RESPONSE).id
        expected_body = ChessGameSerializer(self.awaiting_white_games, many=True, time_with=False).data
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        self.assertEqual(sorted(response.data["response_body"], key=lambda d: d['game']['game_id']), sorted(expected_body, key=lambda d: d['game']['game_id']))
        
        
    
            
class RetrieveSingleGameTest(TestCase):
    def setUp(self):
        timecontrol_1 = TimeControl.objects.create(
            time=1,
            increment=1
        )
    
    
        username_1 = "user_1"
        password_1 = "password_1"
        email_1 = "email1@email.com"
    
        
        user1 = User.objects.create(
            username=username_1,
            email=email_1
        )
        user1.set_password(raw_password=password_1)
        user1.save()
    
        username_2 = "user_2"
        password_2 = "password_2"
        email_2 = "email@email.com"
        
                
        user2 = User.objects.create(
            username=username_2,
            email=email_2
        )
        user2.set_password(raw_password=password_2)
        user2.save()
        
        self.game = Game.objects.create(
            player_w = user1,
            player_b = user2,
            game_time = timecontrol_1
        )
        
        self.client = Client()
        self.client.login(username=username_1, password=password_1)
        self.known_game_id = self.game.game_id
    
    def test_get_game_from_id(self):
        response = self.client.get("/api/game/{}".format(self.known_game_id))
        
        expected_status_code = status.HTTP_200_OK
        expected_response_id = CustomResponse(SuccessResponse.GAME_STATE_RESPONSE).id
        expected_response_body = ChessGameSerializer(self.game, time_with=False).data
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        self.assertEqual(response.data["response_body"], expected_response_body)
    
    
    def test_get_game_from_id(self):
        response = self.client.get("/api/game/wrong_id")
        
        expected_response_code = status.HTTP_404_NOT_FOUND
        expected_response_id = CustomResponse(ErrorResponse.GAME_NOT_FOUND).id
        
        self.assertEqual(response.status_code, expected_response_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
    
    
        
    def test_get_game_from_playername(self):
        response = self.client.get("/api/game/", {
            "player" : "user_1"
        })
        
        expected_response_code = status.HTTP_200_OK
        expected_response_id = CustomResponse(SuccessResponse.GAME_STATE_RESPONSE).id
        expected_response_body = ChessGameSerializer(self.game, time_with=False).data

        
        self.assertEqual(response.status_code, expected_response_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        self.assertEqual(response.data["response_body"], expected_response_body)

    def test_get_game_no_params(self):
        response = self.client.get("/api/game/")
        
        expected_response_code = status.HTTP_200_OK
        expected_response_id = CustomResponse(SuccessResponse.GAME_STATE_RESPONSE).id
        expected_response_body = ChessGameSerializer(self.game, time_with=False).data

        
        self.assertEqual(response.status_code, expected_response_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        self.assertEqual(response.data["response_body"], expected_response_body)
        

    
class SendMoveTest(TestCase):
    def setUp(self):
        timecontrol_1 = TimeControl.objects.create(
            time=1,
            increment=1
        )
        
            
        username_1 = "user_1"
        password_1 = "password_1"
        email_1 = "email1@email.com"
    
        
        user1 = User.objects.create(
            username=username_1,
            email=email_1
        )
        user1.set_password(raw_password=password_1)
        user1.save()
    
        username_2 = "user_2"
        password_2 = "password_2"
        email_2 = "email@email.com"
        
                
        user2 = User.objects.create(
            username=username_2,
            email=email_2
        )
        user2.set_password(raw_password=password_2)
        user2.save()
        
        self.game = Game.objects.create(
            player_w = user1,
            player_b = user2,
            game_time = timecontrol_1,
            game_status = GameStatus.ONGOING
        )
        
        self.client = Client()    
        self.client.login(username=username_1, password=password_1)
    
    def test_valid_move(self):
        response = self.client.post("/api/move/", {
            "move" : "e2e4"
        })
        
        expected_response_code = status.HTTP_200_OK
        expected_response_id = CustomResponse(SuccessResponse.MOVE_MADE).id
        expected_response_body = ChessGameSerializer(self.game, time_with=False).data

        
        # self.assertEqual(response.status_code, expected_response_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        self.assertEqual(response.data["response_body"], expected_response_body)
    
    def test_invalid_move(self):
        response = self.client.post("/api/move/", {
            "move" : "f1f2"
        })
        expected_response_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.INVALID_MOVE).id
        expected_response_body = ChessGameSerializer(self.game, time_with=False).data

        
        self.assertEqual(response.status_code, expected_response_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        self.assertEqual(response.data["response_body"], expected_response_body)
        
class SendMoveNoGameTest(TestCase):
    def setUp(self):
        username_1 = "user_1"
        password_1 = "password_1"
        email_1 = "email1@email.com"
    
        
        user1 = User.objects.create(
            username=username_1,
            email=email_1
        )
        
        user1.set_password(raw_password=password_1)
        user1.save()
        
        self.client = Client()
        self.client.login(username=username_1, password=password_1)
        
    def test_make_move_nogame(self):
        response = self.client.post("/api/move/", {
            "move" : "e2e4"
        })
        
        expected_response_code = status.HTTP_404_NOT_FOUND
        expected_response_id = CustomResponse(ErrorResponse.GAME_NOT_FOUND).id

        
        self.assertEqual(response.status_code, expected_response_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
        
class ResignGameTest(TestCase):
    def setUp(self):
        timecontrol_1 = TimeControl.objects.create(
            time=1,
            increment=1
        )
        
            
        username_1 = "user_1"
        password_1 = "password_1"
        email_1 = "email1@email.com"
    
        
        user1 = User.objects.create(
            username=username_1,
            email=email_1
        )
        user1.set_password(raw_password=password_1)
        user1.save()
    
        username_2 = "user_2"
        password_2 = "password_2"
        email_2 = "email@email.com"
        
                
        user2 = User.objects.create(
            username=username_2,
            email=email_2
        )
        user2.set_password(raw_password=password_2)
        user2.save()
        self.game = Game.objects.create(
            player_w = user1,
            player_b = user2,
            game_time = timecontrol_1,
            game_status = GameStatus.ONGOING
        )
        
        self.client = Client()    
        self.client.login(username=username_1, password=password_1)
    
    def test_resign_game(self):
        response = self.client.post("/api/resign/")
        expected_response_code = status.HTTP_200_OK
        expected_response_id = CustomResponse(SuccessResponse.GAME_OVER).id

        
        self.assertEqual(response.status_code, expected_response_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
    
        
        # for i in range(0, random_n, 2):
        #     player_w = self.users[i]
        #     player_b = self.users[i+1]
        #     time_control = timecontrol_1
        #     self.games.append(self.create_new_game(player_w, player_b, time_control))
        
        # for i in range(random_n, random_n*2, 2):
        #     player_w = self.users[i]
        #     player_b = self.users[i+1]
        #     time_control = timecontrol_2
        #     self.games.append(self.create_new_game(player_w, player_b, time_control))
        
        # for i in range(random_n*2, (random_n*2 + random_n*3) / 2):
        #     player_w = self.users[i]
        #     player_b = None
        #     time_control = timecontrol_1
        #     self.games.append(self.create_new_game(player_w, player_b, time_control))
        
        # for i in range((random_n*2 + random_n*3) / 2, random_n*3):
        #     player_w = None
        #     player_b = self.users[i]
        #     time_control = timecontrol_2
        #     self.games.append(self.create_new_game(player_w, player_b, time_control))
            
        # for i in range(random_n*3, (random_n*3 + random_n*4) / 2):
        #     player_w = None
        #     player_b = self.users[i]
        #     time_control = timecontrol_1
        #     self.games.append(self.create_new_game(player_w, player_b, time_control))
        
        # for i in range((random_n*3 + random_n*4) / 2, random_n*4):
        #     player_w = None
        #     player_b = self.users[i]
        #     time_control = timecontrol_1
        #     self.games.append(self.create_new_game(player_w, player_b, time_control))

        
                
            
            
            
            

        
        
        