# Generated by Django 3.2.8 on 2022-04-05 20:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chess_site', '0005_auto_20220405_2001'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='game_start_time',
            field=models.IntegerField(default=-1),
        ),
    ]
