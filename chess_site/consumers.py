import json
from tokenize import group
from channels.generic.websocket import AsyncWebsocketConsumer, AsyncJsonWebsocketConsumer
from channels.db import database_sync_to_async
from chess_site.chessgamesmanager import ChessGamesManager
from chess_site.chessvalidator import ChessValidator
from chess_site.chessclock import ChessClock
from chess_site.models import Game, GameStatus, MatchmakingStatus, User, MatchmakingInstance
from channels.exceptions import AcceptConnection, DenyConnection
import chess
import time
from djangochannelsrestframework.consumers import AsyncAPIConsumer
from djangochannelsrestframework.decorators import action
from djangochannelsrestframework.observer import observer
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from chess_site.models import schedule



from chess_site.serializers import ChessClockModelSerializer, ChessGameSerializer



class MatchmakingConsumer(AsyncWebsocketConsumer):
    @database_sync_to_async
    def get_user(self, id):
        return User.objects.get(id=id)

    @database_sync_to_async
    def get_user_matchmaking(self, user_id):
        return MatchmakingInstance.objects.get_or_create(user = user_id)
        
    @database_sync_to_async
    def delete_matchmaking_instance(self, matchmaking_instance):
        matchmaking_instance.delete()
        return
        
    async def connect(self):
        if self.scope["user"].is_anonymous: # only allowed logged in users
            await self.close()
            return
    
        self.user_id = self.scope["user"].id
        await self.channel_layer.group_add(
            self.user_id,
            self.channel_name
        )
        matchmaking_instace = await self.get_user_matchmaking(user_id=self.user_id)
        if (matchmaking_instace.matchmaking_status == MatchmakingStatus.COMPLETED):
            await self.close()
            return        


        
class ChessGameConsumer(AsyncJsonWebsocketConsumer):
    @database_sync_to_async
    def get_game(self, game_id):
        try:
            game = Game.objects.get(game_id = game_id)
        except (Game.DoesNotExist):
            print("Game does not exist with id: {}".format(game_id))
            return None
        return game
    
    @database_sync_to_async
    def get_time_control(self, game):
        return game.game_time
    
    @database_sync_to_async
    def get_players(self, game):
        return game.player_w, game.player_b
    
    @database_sync_to_async
    def update_game_fen(self, fen):
        self.game.current_fen = fen
        self.game.save()
        
    @database_sync_to_async
    def update_game_pgn(self, pgn):
        # print("update game pgn: {}".format(pgn))
        self.game.pgn = pgn
    
    @database_sync_to_async
    def connect_player(self, player : User):
        if (player == self.game.player_w):
            self.game.white_connected = True
        elif (player == self.game.player_b):
            self.game.black_connected = True            
    
    @database_sync_to_async
    def commit_game_database_changes(self):
        self.game.save()
    
    @database_sync_to_async
    def update_database(self):
        """
        Updates the game in the database to match that in the ChessValidator.
        """
        
        self.game.pgn = self.validator.get_pgn().dumps()
        self.game.current_fen = self.validator.get_fen()
        
        self.game.time_of_last_move_white = self.clock.get_time_of_last_move_white()
        self.game.time_of_last_move_black = self.clock.get_time_of_last_move_black()

        self.game.time_white = self.clock.get_white_clock()
        self.game.time_black = self.clock.get_black_clock()
        # self.game.game_start_time = self.clock.get_game_start_time()
        # self.game.white_last_time = self.clock.get_white_last_time()
        # self.game.black_last_time = self.clock.get_black_last_time()
        # self.game.time_white_used = self.clock.time_white_used
        # self.game.time_black_used = self.clock.time_black_used
        # self.game.white_clock = self.clock.get_white_clock()
        # self.game.black_clock = self.clock.get_black_clock()
        
        self.game.turn = self.clock.get_turn()
        # print("updating database to\npgn: {}\nfen{}".format(self.validator.get_pgn_moves(), self.validator.get_fen()))
        self.game.save()
    
    @database_sync_to_async
    def create_clock_task(self):
        ChessGamesManager.create_clocks_task(self.game_id)
        
    
    def validate_move(self, old_fen, new_fen, piece, piece_old, piece_new, player):
        return False

    
    def init_validator(self, game):
        return ChessValidator(game)
    
    def init_clock(self, game):
        return ChessClock(game)
    
    @database_sync_to_async
    def start_game(self):
        self.game.game_status = GameStatus.ONGOING
        start_time = time.time_ns() // 1_000_000
        self.game.time_started = start_time
        self.game.white_last_time = start_time
        self.game.black_last_time = start_time
        self.create_clock_task()
        
        
    async def disconnect(self, close_code):
        print("Disconnected")
        
        await self.channel_layer.group_send(self.opponent_game_id, {
                'type' : 'send_message',
                'message' : {'details' : 'OPPONENT_DISCONNECTED'},
                'event' : 'DISCONNECT'
            })
        await self.channel_layer.group_discard(
            self.my_game_id,
            self.channel_name
        ) 
        
        
    async def connect(self):
        """
        Connects user to ChessGame websocket.
        Each users communicates with a seperate group to avoid sending data twice.
        This means that a ChessValidator object exists for each user.

        Returns:
            _type_: _description_
        """
        if self.scope["user"].is_anonymous: # If user is not logged in, close.
            await self.close()
            return
        
        
        self.game_id = self.scope['url_route']['kwargs']['game_id']
        self.user = self.scope['user']
        self.user_id = self.scope['user'].id

        self.game = await self.get_game(self.game_id)
        
        # If game can not be found
        if not self.game: 
            await self.close()
            
        self.game_time = await self.get_time_control(self.game)
            
        self.player_w, self.player_b = await self.get_players(self.game)

        # If user attemping connection is neither player_w nor player_b
        if (self.user_id != self.player_w.id) and (self.user_id != self.player_b.id): 
            return self.close()

        if (self.player_w is None) or (self.player_b is None):
            return self.close()
        
        
        
        # Determine colour of user
        if (self.user_id == self.player_w.id):
            self.colour = "w"
            self.opponent_id = self.player_b.id
        elif (self.user_id == self.player_b.id):
            self.colour = "b"
            self.opponent_id = self.player_w.id
        
        
        self.opponent_game_id = '{}_{}'.format(self.opponent_id, self.game_id)
        self.my_game_id = '{}_{}'.format(self.user_id, self.game_id)
        
        self.validator = self.init_validator(self.game)   
        self.clock = self.init_clock(self.game) 
        
        
        await self.create_clock_task()
        
        
        # if self.clock.get_time_started() == -1:
        #     self.clock.start_clock()
            
        # if self.clock.time_of_last_move_white == -1:
        #     self.clock.time_of_last_move_white = self.clock.get_time_started()
        
        # if self.clock.time_of_last_move_black == -1:
        #     self.clock.time_of_last_move_black = self.clock.get_time_started()
        
        # if (self.clock.time_white == -1):
        #     self.clock.time_white = self.clock.time
        # if (self.clock.time_black == -1):
        #     self.clock.time_black = self.clock.time
        
        await self.connect_player(self.user)
        
        await self.commit_game_database_changes()    

        
        await self.channel_layer.group_add(
            self.my_game_id,
            self.channel_name
        )
        
        
        
        await self.accept()
                                            
        # await self.channel_layer.group_send(self.opponent_game_id, { # send message to opponent
        #     'type': 'make_move',
        #     'message': message,
        #     'event': "MOVE"
        # })
        
        
    async def disconnect(self, close_code):
        print("Disconnected")
        
        await self.channel_layer.group_discard(
            self.my_game_id,
            self.channel_name
        )
        
    async def receive(self, text_data):
        """
        Receive message from WebSocket.
        Get the event and send the appropriate event
        """
        
        response = json.loads(text_data)
        event = response.get("event", None)
        message = response.get("message", None)
        print("RECEIVED MESSAGE event: {}".format(event))
        if event == "CONNECT":
            await self.channel_layer.group_send(self.opponent_game_id, {
                'type' : 'send_message',
                'message' : {'details' : 'OPPONENT_CONNECTED'},
                'event' : 'CONNECT'
            })
            if (self.game.white_connected and self.game.black_connected and self.game.game_status == GameStatus.PENDING): # this probs isnt the best way to do this
                await self.start_game()
                await self.commit_game_database_changes()
                await self.channel_layer.group_send(self.opponent_game_id, {
                    'type' : 'send_message',
                    'event' : 'START',
                    'message' : {'game' : ChessGameSerializer(self.game).data,
                                 'clock' : ChessClockModelSerializer(self.game).data}
                })
            # On connect, retrieve the FEN and PGN from the database for the current game
            # Send message back to sender to INIT_BOARD.
            # database_fen = self.game.current_fen
            # database_pgn = self.game.pgn
            # board = {"fen" : database_fen,
            #          "pgn" : database_pgn}
            
            # if (self.validator.set_board(self.game)):
            #     if (self.colour == "w"):
            #         time = self.clock.get_black_clock()
            #     else:
            #         time = self.clock.get_white_clock()
                
            #     if time == None:
            #         time = self.clock.get_initial_time()
            #     board = {"fen" : self.validator.get_fen(),
            #              "pgn" : self.validator.get_pgn().dumps(),
            #              "pgn_headers" : self.validator.get_pgn_headers_as_dict(),
            #              "time" : time,
            #              "increment" : self.game_time.increment
            #              }
            
            #     print("board {}".format(board))
            #     # print("board valid")
            #     await self.channel_layer.group_send(self.my_game_id, {
            #     'type': 'init_board',
            #     'message': board,
            #     'event': "INIT_BOARD"
            #     })
            # else:
            #     print("board not valid")
        
        if event == 'MOVE':
            # On move event. Check if move is valid. If not return INVALID_MOVE
            # if valid, update ChessValidator & Database then send message to opponent of the move.
            # print("pgn check: {}".format(str(message["pgn"])))
            
            move = self.validator.make_move(message["source"], message["target"], message["pgn"])
            time_on_client_clock = message["time"]
            
            if (self.colour == "w"):
                print("make move white")
                update_clock = self.clock.make_move(self.player_w, time_on_client_clock)
                message["time"] = self.clock.get_white_clock()
            elif (self.colour == "b"):
                print("make move black")
                update_clock = self.clock.make_move(self.player_b, time_on_client_clock)
                message["time"] = self.clock.get_black_clock()
            
            
            if not move: # if not a valid move
                print("INVALID MOVE")      
                await self.channel_layer.group_send(self.my_game_id, {
                'type': 'invalid_move',
                'message': message,
                'event': "INVALID_MOVE"
                })
            else: # is a valid move
                print("valid move")
                await self.update_database() # update database
                
                await self.channel_layer.group_send(self.opponent_game_id, { # send message to opponent
                    'type': 'make_move',
                    'message': message,
                    'event': "MOVE"
                })
                # await self.update_game_fen(message["newFen"])
                # await self.update_game_pgn(self.validator.get_pgn())
                
                # print("pgn : {}".format(self.validator.get_pgn_moves_dict()))
                game_over = self.validator.check_for_game_over()
                if game_over:
                    await self.channel_layer.group_send(self.my_game_id, {
                        'type' : 'game_over',
                        'message' : game_over,
                        'event' : "GAME_OVER"
                    })
                    await self.channel_layer.group_send(self.opponent_game_id, {
                        'type' : 'game_over',
                        'message' : game_over,
                        'event' : "GAME_OVER"
                    })
                    


        if event == 'START':
            # Send message to room group
            await self.channel_layer.group_send(self.opponent_game_id, {
                'type': 'make_move',
                'message': message,
                'event': "START"
            })

        if event == 'END':
            # Send message to room group
            await self.channel_layer.group_send(self.opponent_game_id, {
                'type': 'make_move',
                'message': message,
                'event': "END"
            })
            
        if event == "TEST":
            await self.channel_layer.group_send(self.opponent_game_id, {
                'type' : 'test_message',
                'message' : message,
                'event' : 'TEST'
            })
    
    
################################ MESSAGE RECEIVERS ################################ 

    async def init_board(self, res):
        await self.send(text_data=json.dumps({
        "payload": res,
        }))

    async def make_move(self, res):
        """ Receive make move from opponent, update move and send to Websocket """
        # Send message to WebSocket
        message = res["message"]
        print("Recieving : {}".format(self.colour))
        self.validator.make_move(message["source"], message["target"], message["pgn"])
        if (self.colour == "b"):
            print("make move RECIEVED white")
            update_clock = self.clock.make_move(self.player_w)
            message["time"] = self.clock.get_white_clock()
        elif (self.colour == "w"):
            print("make move RECIEVED black")
            update_clock = self.clock.make_move(self.player_b)
            message["time"] = self.clock.get_black_clock()
        await self.send(text_data=json.dumps({
            "payload": res,
        }))
    
    async def invalid_move(self, res):
        print("inside invalid move")
        await self.send(text_data=json.dumps({
            "payload": res,
        }))
        
    async def game_over(self, res):
        "Receive game over from opponent, send to Websocket"
        await self.send(text_data=json.dumps({
            "payload": res,
        }))
    
    async def test_message(self, res):
        print("Test message called")
        
        await self.send(text_data=json.dumps({
            "payload" : res 
        }))
        
    async def send_message(self, res):
        print("send message called")
        print(res)
        res['message_info'] = {
            'time_sent' : time.time_ns() // 1_000_000
        }
        await self.send(text_data=json.dumps({
            "payload" : res
        }))
