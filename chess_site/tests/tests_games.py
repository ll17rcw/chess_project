"""
Tests in this file are designed to emulate typical scenarios of users playing chess games.
Scenarios are isolated to outline the individual test, with some being more generic than others.
"""

from django.test import TestCase, Client
from chess_site.models import Game
from chess_site.models import User, Game


class CreateGamesTest(TestCase):
    
    @classmethod
    def setUp(self):
        self.client = Client()
        self.user1 = User.objects.create(username="user1", password="password")
        self.user2 = User.objects.create(username="user2", password="password")
        self.user3 = User.objects.create(username="user3", password="password")
        self.user4 = User.objects.create(username="user4", password="password")
        self.user5 = User.objects.create(username="user5", password="password")
        self.user6 = User.objects.create(username="user6", password="password")
    
    