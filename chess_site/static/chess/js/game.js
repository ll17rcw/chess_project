
// static/js/game.js

// const { Clock } = require("chess-clock");

// const { Chess } = require("./chess");

var gameId = document.getElementById("myBoard").getAttribute("game_id")
var connectionString = 'ws://' + window.location.host + '/ws/play/' + gameId + '/';
var board = null
var playerColour = document.getElementById("myBoard").getAttribute("player_colour")
var game = new Chess()
var whiteSquareGrey = '#a9a9a9'
var blackSquareGrey = '#696969'
var moveCount = 0;
var fenHistory = []
var boardReady = false;
var whiteClockDisplay = document.querySelector('#whiteClock')
var blackClockDisplay = document.querySelector('#blackClock')
var whiteClock = null;
var blackClock = null;

var timeObj = null;




/**
 * Sets the board and game to the
 * @param {string} fen 
 */
function setBoard(fen, pgn, pgn_headers){
  try{
    if (!game.load_pgn(pgn)){
      for (var pgn_header in pgn_headers){
        // console.log(String(pgn_header) + " " + String(pgn_headers[pgn_header]))
        game.header(String(pgn_header), String(pgn_headers[pgn_header]))
      }
    }
    board.position(game.fen(), false)
    updateHistoryTable()
    // console.log("here")
    // console.log(game.history())
    boardReady = true
    if (game.turn() == playerColour){
      timer.start()
    }
  }
  catch (err){
    console.log("Error retreiving board position from server")
    console.log(err)
    boardReady = false
  }


}

function formatWhiteDisplay(minutes, seconds, deciseconds) {
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;
  deciseconds = deciseconds < 10 ? + deciseconds : deciseconds;
  whiteClockDisplay.textContent = minutes + ':' + seconds + '.' + deciseconds;
}

function formatBlackDisplay(minutes, seconds, deciseconds) {
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;
  deciseconds = deciseconds < 10 ? + deciseconds : deciseconds;
  blackClockDisplay.textContent = minutes + ':' + seconds + '.' + deciseconds;
}

function setClocks(whiteClockTime, blackClockTime, increment){
  // time_deciseconds = time_minutes
  // // console.log("recieved : " + time_deciseconds + " and " + increment)
  // timer = new Clock(time_deciseconds)
  // timeObj = Clock.parse(time_deciseconds)
  // format(timeObj.minutes, timeObj.seconds, timeObj.deciseconds);
  // timer.onTick(format);

  console.log("White clock time: " + whiteClockTime);
  console.log("Black clock time: " + blackClockTime);
  
  whiteClock = new Clock(whiteClockTime);
  blackClock = new Clock(blackClockDisplay);
  white_timeObj = Clock.parse(whiteClockTime);
  black_timeObj = Clock.parse(blackClockTime);

  
  formatWhiteDisplay(white_timeObj.minutes, white_timeObj.seconds, black_timeObj.deciseconds);
  formatBlackDisplay(black_timeObj.minutes, black_timeObj.seconds, black_timeObj.deciseconds);

  whiteClock.onTick(formatWhiteDisplay);
  blackClock.onTick(formatBlackDisplay);
  
}

function startClocks(turn){
  if (turn == 'w'){
    whiteClock.start()
  }
  else if (turn == 'b'){
    blackClock.start()
  }
}

function setMoveList(pgn){
  // console.log(pgn)
}


function removeGreySquares () {
  $('#myBoard .square-55d63').css('background', '')
}

function greySquare (square) {
  var $square = $('#myBoard .square-' + square)

  var background = whiteSquareGrey
  if ($square.hasClass('black-3c85d')) {
    background = blackSquareGrey
  }

  $square.css('background', background)
}

function onDragStart (source, piece) {
  if (!boardReady) return false // if game is not ready

  if (game.game_over()) return false // if game is over


  if (game.turn() != playerColour){ //if its not this players turn
    return false
  }
  

  // or if it's not that side's turn
  if ((game.turn() === 'w' && piece.search(/^w/) === -1) || // if whites turn and piece moved is not white (-1) or
      (game.turn() === 'b' && piece.search(/^b/) === -1)) { // if blacks turn and piece moved is not black (-1)
    return false
  }
}


function updateHistoryTable(){
  // console.log(game.history())
  var move_history = game.history({verbose : true})
  // console.log(typeof(history))
  
  data = ''
  // for (var move in move_history){
  //   console.log(move_history[move])
  //   var san = move_history[move].san
  //   data += '<tr> <th scope="row">'+index+'</th><td>'+san+'</td>'
  // }
  move_index = 1;
  if (move_history.length % 2 === 0){
    // console.log("is divisible by 2")
    for (let i = 0; i < move_history.length-1; i+=2){
      var san = move_history[i].san
      var next_san = move_history[i+1].san
      data += '<tr> <th scope="row">'+move_index+'</th><td>'+san+'</td><td>'+next_san+'</td>'
      move_index++
    }
  }
  else{
    // console.log("is NOT divisible by 2")
    for (let i = 0; i < move_history.length; i+=2){
      if (i == move_history.length-1){
        var san = move_history[i].san
        data += '<tr> <th scope="row">'+move_index+'</th><td>'+san+'</td>'
      }
      else{
        var san = move_history[i].san
        var next_san = move_history[i+1].san
        data += '<tr> <th scope="row">'+move_index+'</th><td>'+san+'</td><td>'+next_san+'</td>'
        move_index++
      }
    }
  }

  $('#move-list').html(data)
}

function checkMove(oldFen, source, target){
$.ajax({
  url: "/api/validmove",
  type : "get",
  data : {
    "old_fen" : oldFen,
    "source" : source,
    "target" : target
  },
  success : function(response){
    return true
  },
  error: function(response){
    return false
  }
})
}

function onDrop (source, target, piece, newPos, oldPos, orientation) {
  removeGreySquares()

  oldFen = game.fen()
  // see if the move is legal
  var move = game.move({
    from: source,
    to: target,
    promotion: 'q' // NOTE: always promote to a queen for example simplicity
  })

  console.log(oldFen + " " + source + " " + target)
  isValidMove = checkMove(oldFen, source, target)
  if (!isValidMove) return 'snapback'
  if (move === null) return 'snapback'
  if (game.turn() != playerColour){
    timer.pause()
  }
  newFen = game.fen()
  pgn = game.pgn()


  moveCount += 1;
  let event = "MOVE"
  let message = {
    "oldPos" : oldPos,
    "newPos" : newPos,
    "oldFen" : oldFen,
    "newFen" : newFen,
    "piece" : piece,
    "source" : source,
    "target" : target,
    "player" : playerColour,
    "moveCount" : moveCount,
    "pgn" : pgn,
    "time" : time
  }
  
  sendMessage(event, message, playerColour);

  // console.log("after sendmessage")
  updateHistoryTable()
}

function revertMove(oldFen){
  board.position(oldFen);
  game.load(oldFen)

}

function onMouseoverSquare (square, piece) {

  if (!boardReady) return // if the board is not ready

  if (game.turn() !== playerColour){ //if its not this players turn
    return
  }
  
  // get list of possible moves for this square
  var moves = game.moves({
    square: square,
    verbose: true
  })

  // exit if there are no moves available for this square
  if (moves.length === 0) return

  // highlight the square they moused over
  greySquare(square)

  // highlight the possible squares for this piece
  for (var i = 0; i < moves.length; i++) {
    greySquare(moves[i].to)
  }
}

function onMouseoutSquare (square, piece) {
  removeGreySquares()
}

function onSnapEnd () {
  board.position(game.fen())
}


function onChange (oldPos, newPos) {

  // let event = "MOVE"
  // let message = {
  //   "oldPos" : oldPos,
  //   "newPos" : newPos,
  //   "player" : playerColour
  // }
  
  // sendMessage(event, message, playerColour);
  
}


function madeMove(oldPos, newPos, source, target){

  // oldPosDict = new dispatchEvent()
  
  if (JSON.stringify(board.position()) !== JSON.stringify(oldPos)){
    return;
  }

  // console.log("is equal");
  // console.log("source: " + source);
  // console.log("target: " + target);

  var move = game.move({
    from: source,
    to: target,
    promotion: 'q' // NOTE: always promote to a queen for example simplicity TODO: FIX THIS
  })

  if (move === null){
    // console.log("is null")
    return 'snapback';
  }
  board.position(newPos, false)
  if (game.turn() == playerColour){
    timer.start(timer.getTime());
  }


  updateHistoryTable()



}



// on load, check if current game exists
if (playerColour == "w"){
  orient = "white"
}
else if (playerColour == "b"){
  orient = "black"
}

var config = {
  draggable: true,
  position: 'start',
  orientation : orient,
  onDragStart: onDragStart,
  onDrop: onDrop,
  onMouseoutSquare: onMouseoutSquare,
  onMouseoverSquare: onMouseoverSquare,
  onSnapEnd: onSnapEnd,
  onChange, onChange,
  pieceTheme: '/static/chess/img/chesspieces/wikipedia/{piece}.png'

}
board = Chessboard('myBoard', config)

// document.getElementById("testbtn").onclick = function() {myFunction()};



// document.getElementById("countdown").onclick = function(){
// var display = document.querySelector('#time'),
//   timer = new Clock(5),
//   timeObj = Clock.parse(5);

// format(timeObj.minutes, timeObj.seconds)


// timer.onTick(format);
// console.log("time start")
// timer.start();

// function format(minutes, seconds) {
//   minutes = timeObj.minutes < 10 ? "0" + timeObj.minutes : timeObj.minutes;
//   seconds = timeObj.seconds < 10 ? "0" + timeObj.seconds : timeObj.seconds;
//   display.textContent = minutes + ':' + seconds;
// }


// }



// function init_clock(time) {


// };

// document.getElementById("countdown").addEventListener('click', function () {
//   time = timer.getTime()
//   if (time == null){
//     timer.start();
//   } 
//   else{
//     timer.start(time);
//   }
  
// });

// document.getElementById("pause_countdown").addEventListener('click', function() {
// timer.pause();
// });

function myFunction() {
  sendMessage("TEST", "Test button pressed")
}




function loadGame(){
  $.ajax({  
    type : 'GET',
    url : '/api/game',
    dataType : 'json',
    success : function(res){
      console.log(res);
      // var game_id = res["game_id"]
      // var current_fen = res["current_fen"]
      // var pgn = res["pgn"]
      // var game_status = res["game_status"]
      // var time_started = res["time_started"]
      // setBoard(current_fen, pgn);
      console.log("before game info")
      var gameInfo = res['game']
      var clockInfo = res['clock']
      var messageInfo = res['message_info']

      console.log("after game info")
      setBoard(gameInfo['current_fen'], gameInfo['pgn'])

      setClocks(clockInfo['white_clock'], clockInfo['black_clock'])
      
      if (gameInfo['game_status'] == 2){
        startClocks(gameInfo['turn'])
      }

      console.log("here");
    },
    error: function(res){
        console.log("Error connecting to server")
        console.log(res)
        
    }
})
}

$(document).ready(function(){
  connect(connectionString);
  console.log("here");
  // console.log("game: " + game_response)
  loadGame();

})