# Generated by Django 3.2.8 on 2022-04-26 11:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chess_site', '0014_auto_20220426_1108'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='black_connected',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='game',
            name='white_connected',
            field=models.BooleanField(default=False),
        ),
    ]
