
from chess_site.data.data import SITE_DATA
from django.urls import path, include
import types
from chess_site.endpoints.endpoints import Endpoint

class UrlsGenerator():
    
    # def construct_path(handler_path, function):
    #     return path(handler_path, function)
    
    

    def construct_urlpatterns(prefix):
        
        print("IN CONSTRUCT URLPATTERNS\n\n\n")
        endpoints_key = "{}_endpoints".format(prefix)
        endpoints_data = SITE_DATA["endpoints"][endpoints_key]
        print(endpoints_data)
        urlpatterns = []
        
        for key, value in endpoints_data.items():
            # func = getattr(module, value)
            # print(module)
            # func = getattr(module, value)
            print("\n\n{}\n{}\n\n".format(key, value))
            # urlpatterns.append(path(key, func))
            yield key,value
        # with open(endpoints_filepath) as f:
        #     endpoints_data = json.load(f)
        #     for key, value in endpoints_data.items():
        #         return path(key, value)

        # print("\n\n======URL PATTERNS====== \n\n")
        # print(urlpatterns)
        # print("\n\n\n\n")

        # return urlpatterns

    def get_name_from_module(module):
        return module.__name__.split(".")[-1]
    
    def construct_urlpatterns_r(module, prefix = None):
        if prefix is None:
            prefix = UrlsGenerator.get_name_from_module(module)
        print("IN CONSTRUCT URLPATTERNS\n\n\n")
        endpoints_key = "{}_endpoints".format(prefix)
        endpoints_data = SITE_DATA["endpoints"][endpoints_key]
        print(endpoints_data)
        urlpatterns = []
        
        for endpoint, endpoint_handler in endpoints_data.items():
            # func = getattr(module, value)
            # print(module)
            func = getattr(module, endpoint_handler)
            print("\n\n{}\n\n".format(endpoint))
            # urlpatterns.append(path(key, func))
            # endpoint_obj = Endpoint(endpoint_name=str(endpoint),
            #                         endpoint_data=endpoint_data,
            #                         module=module)
            # func = endpoint_obj.get_handler()
            if isinstance(func, types.FunctionType):
                urlpatterns.append(path(endpoint, func))
            else:
                urlpatterns.append(path(endpoint, func.as_view()))
            
            
        # with open(endpoints_filepath) as f:
        #     endpoints_data = json.load(f)
        #     for key, value in endpoints_data.items():
        #         return path(key, value)

        # print("\n\n======URL PATTERNS====== \n\n")
        # print(urlpatterns)
        # print("\n\n\n\n")

        return urlpatterns
    
    def get_all_endpoints():
        return_endpoints = []
        endpoint_files = SITE_DATA["endpoints"]
        for endpoint_file in endpoint_files:
            if str(endpoint_file).startswith("views"):
                prefix = ""
            else:
                prefix = str(endpoint_file).split("_")[0]
            
            endpoints = endpoint_files[endpoint_file]
            for endpoint in endpoints:
                return_endpoints.append(prefix + "/" + endpoint)
                
        return return_endpoints
                
        

        
        
        
# urlpatterns = UrlsGenerator.construct_urlpatterns("api")

        
        

        