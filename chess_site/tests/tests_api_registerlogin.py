from django.test import TestCase, Client
from chess_site.models import User, Game
from rest_framework import status

from chess_site.responses import CustomResponse, ErrorResponse, SuccessResponse

class SucessfulRegistrationTest(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_register_new_user(self):
        username = 'user1'
        email = "user1@email.com"
        password = 'user1pass'
        response = self.client.post("/api/register/", {
            'username' : username,
            'email' : email,
            'password' : password
        })
        
        user_exists = User.objects.filter(username=username).exists()
        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["response_id"], CustomResponse(SuccessResponse.USER_CREATED).id)
        self.assertTrue(user_exists)

class ReRegistrationTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user1 = User.objects.create(
            username="user1",
            password = "user1pass",
            email = "user1@email.com"
        )
    
    def test_reregister_user_username(self):
        username = 'user1'
        password = 'user1newpass'
        email = "user1new@email.com"
        
        # expected_user = User.objects.create_user(username=username, email=email, password=password)
        user_exists = User.objects.filter(username=username).exists()
        
        self.assertTrue(user_exists) # Ensure user is not added before post call.
    
    
        response = self.client.post("/api/register/", {
            'username' : username,
            'email' : email,
            'password' : password
        })
        
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["response_id"], CustomResponse(ErrorResponse.USER_EXISTS).id)
        self.assertTrue(user_exists)

class RegisterNotEnoughInformationTest(TestCase):
    def test_register_missing_username(self):
        password = "user1pass"
        email = "user1@email.com"
        

        response = self.client.post("/api/register/", {
            'email' : email,
            'password' : password
        })
                
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.NOT_ENOUGH_USER_INFO).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
    def test_register_missing_email(self):
        username = "user1"
        password = "user1pass"
        
        response = self.client.post("/api/register/", {
            "username" : username,
            "password" : password
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.NOT_ENOUGH_USER_INFO).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
    def test_register_missing_email(self):
        username = "user1"
        email = "user1pass"
        
        response = self.client.post("/api/register/", {
            "username" : username,
            "email" : email
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.NOT_ENOUGH_USER_INFO).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)

class LoginSuccessfulTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user1 = User.objects.create(
            username="user1",
            password = "user1pass",
            email = "user1@email.com"
        )
    
    def test_successful_login(self):
        username = "user1"
        password = "user1pass"
        
        response = self.client.post("/api/login/",{
            "username" : username,
            "password" : password
        })
        
        expected_status_code = status.HTTP_200_OK
        expected_response_id = CustomResponse(SuccessResponse.USER_CREATED).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
    
class LoginUnsuccessfulTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user1 = User.objects.create(
            username="user1",
            password = "user1pass",
            email = "user1@email.com"
        )
        
    def test_unsuccessful_login_incorrect_password(self):
        username = "user1"
        password = "incorrectpassword"
        
        response = self.client.post("/api/login/",{
            "username" : username,
            "password" : password
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.LOGIN_FAILED).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
    def test_unsuccessful_login_incorrect_username(self):
        username = "incorrectusername"
        password = "user1pass"
        
        response = self.client.post("/api/login/",{
            "username" : username,
            "password" : password
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.LOGIN_FAILED).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
    def test_unsuccessful_login_incorrect_username_and_password(self):
        username = "incorrectusername"
        password = "incorrectpassword"
        
        response = self.client.post("/api/login/",{
            "username" : username,
            "password" : password
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.LOGIN_FAILED).id
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
        
    
    

    

        
        
        
        
        
        
        