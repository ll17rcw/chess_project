
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

class ChessMessages():
    """
    Class that contains static methods for sending messages to ChessConsumer instances.
    """
    
    
    @staticmethod
    def send_message(game, message):
        return        
    
    @staticmethod
    def alert_game_over(game, winner):
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            str('{}_{}'.format(game.player_w.id, game.game_id)).replace("-", ""),
            {'type' : 'send_message',
            'event' : 'GAME_OVER',
            'message' : {'winner' : winner,
                         'reason' : 'TIMEOUT'}}
        )
        async_to_sync(channel_layer.group_send)(
            str('{}_{}'.format(game.player_b.id, game.game_id)).replace("-", ""),
            {'type' : 'send_message',
            'event' : 'GAME_OVER',
            'message' : {'winner' : winner,
                         'reason' : 'TIMEOUT'}}
        )
    