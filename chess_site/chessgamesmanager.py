
from chess_site.chessclock import ChessClock
from celery import shared_task
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from chess_site.models import Game, TimeControl, schedule, GameStatus
import json
from channels.layers import get_channel_layer

from asgiref.sync import async_to_sync


class ChessGamesManager():
    
    TASK_PREFIX = "chessgame_"
    
    @staticmethod
    def alert_game_over(game, winner):
        channel_layer = get_channel_layer()

        
        async_to_sync(channel_layer.group_send)(
            str('{}_{}'.format(game.player_w.id, game.game_id)).replace("-", ""),
            {'type' : 'send_message',
            'event' : 'GAME_OVER',
            'message' : {'winner' : winner,
                         'reason' : 'TIMEOUT'}}
        )
        async_to_sync(channel_layer.group_send)(
            str('{}_{}'.format(game.player_b.id, game.game_id)).replace("-", ""),
            {'type' : 'send_message',
            'event' : 'GAME_OVER',
            'message' : {'winner' : winner,
                         'reason' : 'TIMEOUT'}}
        )
    

    
    
    def create_clocks_task(game_id):
        task = PeriodicTask.objects.get_or_create(
                interval = schedule,
                name = '{}{}'.format(ChessGamesManager.TASK_PREFIX, game_id),
                task = 'chess_site.tasks.update_clocks',
                args=json.dumps([game_id]),
            )
        return task
    
    @staticmethod
    def remove_clocks_task(game_id : str):
        try:
            task = PeriodicTask.objects.get(name=game_id)
        except PeriodicTask.DoesNotExist:
            print("task does not exist")
        task.delete()        
    
    
    @staticmethod
    def cleanup_tasks():
        tasks = PeriodicTask.objects.all()
        for task in tasks:
            game = Game.objects.get(id=task.name)
            if game.game_status == GameStatus.COMPLETED:
                task.delete()        
        
    # def get_game_time(game_time):
    #     time, increment = game_time.split("+", 2)
    #     game_time_object = TimeControl.objects.get(time=time, increment=increment)
    #     return game_time_object

                
    def create_new_game(user, colour, time, increment):
        print("TIME: {} INCREMENT: {}".format(time, increment))
        time_control = TimeControl.objects.get(time=time, increment=increment)
        if colour == "w":
            new_game = Game.objects.create(
                player_w = user,
                player_b = None,
                game_time=time_control
            )
        elif colour == "b":
            new_game = Game.objects.create(
                player_w = None,
                player_b=user,
                game_time=time_control
            )
        else:
            new_game = None
        return new_game
        
        
        
        