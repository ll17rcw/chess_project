from django.test import TestCase, Client
from chess_site.models import User, Game
from rest_framework import status

from chess_site.responses import CustomResponse, ErrorResponse, SuccessResponse

class FENStringValidationTest(TestCase):
    def setUp(self):
        self.client = Client()
        
    def test_valid_fen(self):
        response = self.client.get("/api/validatefen", {
            "FEN" : "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
        })
        
        expected_status_code = status.HTTP_200_OK
        expected_response_id = CustomResponse(SuccessResponse.VALID_FEN)
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
    def test_invalid_fen(self):
        response = self.client.get("/api/validatefen", {
            "FEN" : "PPPPPPPP/8/8/8/8/8/8/pppppppp w - - 0 1"
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.INVALID_FEN)
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)

class ChessMoveValidationTest(TestCase):
    def setUp(self):
        self.client = Client()
        
    def test_valid_fen(self):
        response = self.client.get("/api/validatemove", {
            "FEN" : "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
            "move" : "e2e4"
        })
        
        expected_status_code = status.HTTP_200_OK
        expected_response_id = CustomResponse(SuccessResponse.VALID_FEN)
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
        
    def test_invalid_fen(self):
        response = self.client.get("/api/validatefen", {
            "FEN" : "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
            "move" : "f1f2"
        })
        
        expected_status_code = status.HTTP_400_BAD_REQUEST
        expected_response_id = CustomResponse(ErrorResponse.INVALID_MOVE)
        
        self.assertEqual(response.status_code, expected_status_code)
        self.assertEqual(response.data["response_id"], expected_response_id)
